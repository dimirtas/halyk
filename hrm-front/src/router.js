import { createRouter, createWebHashHistory } from "vue-router";

import Home from "./views/home-page";
import Profile from "./views/profile-page";
import Tasks from "./views/tasks-page";
import Users from "./views/users-list";
import User from "./views/user-detail-view"
import Roles from "./views/role/roles-page";
import Сompanies from "./views/company/companies-page";
import Сities from "./views/city/cities-page";
import Benefits from "./views/benefit/benefits-page";
import Departments from "./views/department/departments-page";
import Divisions from "./views/division/divisions-page";
import defaultLayout from "./layouts/side-nav-inner-toolbar";
import simpleLayout from "./layouts/single-card";
import store from "./store/store";
import applications from "./views/application/applications-page.vue";
import application from "./views/application/application-detail-page.vue";
import abilities from "./views/ability/abilities-page";
import qualities from "./views/quality/qualities-page";
import knowledges from "./views/knowledge/knowledges-page";
import skills from "./views/skill/skills-page";
import vacancies from "./views/vacancy/vacancies-page.vue";
import vacancy from "./views/vacancy/vacancy-detail-page.vue";
import Resumes from "./views/resume/resumes-list.vue"
import Resume from "./views/resume/resume-detail-view.vue"


function loadView(view) {
    return () =>
        import ( /* webpackChunkName: "login" */ `./views/${view}.vue`)
}

const router = new createRouter({
    routes: [{
            path: "/home",
            name: "home",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Home
        },

        {
            path: "/profile",
            name: "profile",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Profile
        },
        {
            path: "/vacancies",
            name: "vacancies",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: vacancies
        },
        {
            path: "/vacancy",
            name: "vacancy",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            children: [{
                    path: "create",
                    name: "createVacancy",
                    component: vacancy
                },
                {
                    path: "view/:id",
                    name: "detailVacancy",
                    component: vacancy
                }
            ],
            component: vacancy
        },
        {
            path: "/applications",
            name: "applications",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: applications
        },
        {
            path: "/application",
            name: "application",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            children: [{
                    path: "create",
                    name: "createApplication",
                    component: application
                },
                {
                    path: "view/:id",
                    name: "detailApplication",
                    component: application
                }
            ],
            component: application
        },
        {
            path: "/user",
            name: "user",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            children: [{
                    path: "create",
                    name: "createUser",
                    component: User
                },
                {
                    path: "view/:id",
                    name: "detaillUser",
                    component: User
                }
            ],
            component: User
        },
        {
            path: "/tasks",
            name: "tasks",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Tasks
        },
        {
            path: "/users",
            name: "users",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Users
        },
        {
            path: "/roles",
            name: "roles",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Roles
        },
        {
            path: "/companies",
            name: "companies",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Сompanies
        },
        {
            path: "/cities",
            name: "cities",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Сities
        },
        {
            path: "/benefits",
            name: "benefits",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Benefits
        },
        {
            path: "/departments",
            name: "departments",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Departments
        },
        {
            path: "/divisions",
            name: "divisions",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Divisions
        },
        {
            path: "/abilities",
            name: "abilities",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: abilities
        },
        {
            path: "/qualities",
            name: "qualities",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: qualities
        },
        {
            path: "/knowledges",
            name: "knowledges",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: knowledges
        },
        {
            path: "/skills",
            name: "skills",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: skills
        },
        {
            path: "/resume",
            name: "resume",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            children: [{
                path: "view/:id",
                name: "detailResume",
                component: Resume
            }],
            component: Resume
        },
        {
            path: "/resumes",
            name: "resumes",
            meta: {
                requiresAuth: true,
                layout: defaultLayout
            },
            component: Resumes
        },
        {
            path: "/login-form",
            name: "login-form",
            meta: {
                requiresAuth: false,
                layout: simpleLayout,
                title: "Sign In"
            },
            component: loadView("login-form")
        },
        {
            path: "/reset-password",
            name: "reset-password",
            meta: {
                requiresAuth: false,
                layout: simpleLayout,
                title: "Reset Password",
                description: "Please enter the email address that you used to register, and we will send you a link to reset your password via Email."
            },
            component: loadView("reset-password-form")
        },
        {
            path: "/create-account",
            name: "create-account",
            meta: {
                requiresAuth: false,
                layout: simpleLayout,
                title: "Регистрация"
            },
            component: loadView("create-account-form"),
        },
        {
            path: "/change-password/:recoveryCode",
            name: "change-password",
            meta: {
                requiresAuth: false,
                layout: simpleLayout,
                title: "Change Password"
            },
            component: loadView("change-password-form")
        },
        {
            path: "/",
            redirect: "/home"
        },
        {
            path: "/recovery",
            redirect: "/home"
        },
        {
            path: "/:pathMatch(.*)*",
            redirect: "/home"
        }
    ],
    history: createWebHashHistory()
});

router.beforeEach((to, from, next) => {


    if (to.name === "login-form" && store.getters["auth/loggedIn"]) {
        next({ name: "home" });
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters["auth/loggedIn"]) {
            next({
                name: "login-form",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;