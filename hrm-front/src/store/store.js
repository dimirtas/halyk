import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
const plugins = [createPersistedState()];


import user from "./models/user";
import role from "./models/role";
import auth from "./models/auth";
import company from "./models/company";
import city from "./models/city";
import benefit from "./models/benefit";
import department from "./models/department";
import division from "./models/division";
import application from "./models/application";
import ability from "./models/ability";
import quality from "./models/quality";
import knowledge from "./models/knowledge";
import skill from "./models/skill";
import vacancy from "./models/vacancy";
import resume from "./models/resume"

export default createStore({
    modules: {
        user,
        role,
        auth,
        company,
        city,
        benefit,
        department,
        division,
        application,
        ability,
        quality,
        knowledge,
        skill,
        vacancy,
        resume
    },
    plugins
});