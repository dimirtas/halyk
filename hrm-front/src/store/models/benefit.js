import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        benefit: null,
        benefits: null,
        benefitsByCompanyId: null
    },
    getters: {
        benefit(state) {
            return state.benefit;
        },
        benefits(state) {
            return state.benefits;
        },
        benefitsByCompanyId(state) {
            return state.benefitsByCompanyId;
        }
    },
    mutations: {
        setBenefit(state, data) {
            state.benefit = data;
        },
        setBenefits(state, data) {
            state.benefits = data;
        },
        setBenefitsByCompanyId(state, data) {
            state.benefitsByCompanyId = data;
        }
    },
    actions: {
        getAllBenefits(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Benefits")
                    .then((response) => {
                        context.commit("setBenefits", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getBenefit({ commit }, benefitId) {
            try {
                const { data } = await axios.get("api/Benefits/" + benefitId);
                console.log(data);
                commit("setBenefit", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getBenefitsByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Benefits/GetByCompanyId/" + companyId);
                commit("setBenefitsByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteBenefit({ dispatch }, benefitId) {
            try {
                await axios.delete("api/Benefits/Delete?id=" + benefitId);
                await dispatch("getAllBenefits");
            } catch (error) {
                console.log(error);
            }
        },

        async updateBenefit({ dispatch }, benefit) {
            try {
                await axios.put("api/Benefits/update", benefit);
                await dispatch("getAllBenefits");
            } catch (error) {
                console.log(error);
            }
        },

        async createBenefit({ dispatch }, benefit) {
            try {
                console.log(benefit)
                await axios.post("api/Benefits/create", benefit);
                await dispatch("getAllBenefits");
            } catch (error) {
                console.log(error);
            }
        }

    }
};