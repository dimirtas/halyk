﻿import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        skill: null,
        skills: null,
        skillsByCompanyId: null
    },
    getters: {
        skill(state) {
            return state.skill;
        },
        skills(state) {
            return state.skills;
        },
        skillsByCompanyId(state) {
            return state.skillsByCompanyId;
        }
    },
    mutations: {
        setSkill(state, data) {
            state.skill = data;
        },
        setSkills(state, data) {
            state.skills = data;
        },
        setSkillsByCompanyId(state, data) {
            state.skillsByCompanyId = data;
        }
    },
    actions: {
        getAllSkills(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Skills")
                    .then((response) => {
                        context.commit("setSkills", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getSkill({ commit }, skillId) {
            try {
                const { data } = await axios.get("api/Skills/" + skillId);
                console.log(data);
                commit("setSkill", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getSkillsByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Skills/GetByCompanyId/" + companyId);
                commit("setSkillsByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteSkill({ dispatch }, skillId) {
            try {
                await axios.delete("api/Skills/Delete?id=" + skillId);
                await dispatch("getAllSkills");
            } catch (error) {
                console.log(error);
            }
        },

        async updateSkill({ dispatch }, skill) {
            try {
                await axios.put("api/Skills/update", skill);
                await dispatch("getAllSkills");
            } catch (error) {
                console.log(error);
            }
        },

        async createSkill({ dispatch }, skill) {
            try {
                console.log(skill)
                await axios.post("api/Skills/create", skill);
                await dispatch("getAllSkills");
            } catch (error) {
                console.log(error);
            }
        }

    }
};