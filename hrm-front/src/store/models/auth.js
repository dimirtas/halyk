import axios from "@/axios/requestAxios";
 

export default {
    namespaced: true,
    state: {
        user: localStorage.getItem('user') || Object,
        refreshToken: localStorage.getItem('refreshToken') || null,
        accessToken: localStorage.getItem('accessToken') || null,
        lifeTimeAccessToken: localStorage.getItem('lifeTimeAccessToken') || null,
        email: localStorage.getItem('email') || null
    },
    getters: {
        getRefreshToken(state) {
          if (state.refreshToken == null) return null;
          return state.refreshToken;
        },
        getAccessToken(state) {
            if (state.accessToken == null) return null;
            return state.accessToken;
        },
        loggedIn() {
          if (localStorage.getItem('user') == null) return false;
          return true;
        },
        getLifeTimeAccessToken(state) {
          return state.lifeTimeAccessToken;
        },
        getUser(state) {
          return state.user;
        },
        getId(state){
          if (state.user == null) return null;
          return state.user.id;
        },
    },
    mutations: {
        setJwtData(state, { user, accessToken, refreshToken, lifeTimeAccessToken }) {
          localStorage.setItem('user', user);
          localStorage.setItem('accessToken', accessToken);
          localStorage.setItem('refreshToken', refreshToken);
          localStorage.setItem('lifeTimeAccessToken', lifeTimeAccessToken);
          state.user = user;
          state.lifeTimeAccessToken = lifeTimeAccessToken;
          state.accessToken = accessToken;
          state.refreshToken = refreshToken;
        },
        destroyJwtData(state) {
          state.user = null;
          state.refreshToken = null;
          state.accessToken = null;
          state.lifeTimeAccessToken = null;
        },
        destroyJwtLocalStorage() {
          localStorage.removeItem('user');
          localStorage.removeItem('refreshToken');
          localStorage.removeItem('accessToken');
          localStorage.removeItem('lifeTimeAccessToken');
        }
      },
      actions:{
        async Login(context, formData) {
          return new Promise((resolve, reject) => {
            try {
              axios.post("api/Auth/Login",{
                email: formData.email,
                password: formData.password
              })
              .then(function(response){
                const refreshToken = response.data.refreshToken;
                const accessToken = response.data.accessToken;
                const lifeTimeAccessToken = response.data.lifeTimeAccessToken;
                const user = response.data.user;
    
                context.commit('setJwtData', {user, accessToken, refreshToken, lifeTimeAccessToken});
                resolve ({
                  isOk: true,
                  data: context.getUser
                });
              })
              
            }
            catch {
              reject({
                isOk: false,
                message: "Authentication failed"
              });
            }
          });
        },

        async Logout(context){
          return new Promise((resolve, reject) => {
            try {
              axios.post("api/Auth/Logout?id=" + context.getters.getId)
              .then(function(){
                context.commit('destroyJwtData');
                context.commit('destroyJwtLocalStorage');
                resolve();
              })
            }
            catch{
              reject();
            }
          });
        }
      } 
}