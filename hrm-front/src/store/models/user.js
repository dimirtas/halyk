import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        user: null,
        users: null,
        usersByCompanyId: null
    },
    getters: {
        user(state) {
            return state.user;
        },
        users(state) {
            return state.users;
        },
        userByRole: (state) => (roleName) => {
            return state.users.filter(f => f.roleList.includes(roleName));
        },
        usersByCompanyId(state) {
            return state.usersByCompanyId;
        }
    },
    mutations: {
        setUser(state, data) {
            state.user = data;
        },
        setUsers(state, data) {
            state.users = data;
        },
        setUsersByCompanyId(state, data) {
            state.usersByCompanyId = data;
        }
    },
    actions: {
        getAllUsers(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Users")
                    .then((response) => {
                        context.commit("setUsers", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getUser({ commit }, userId) {
            try {
                const { data } = await axios.get("api/Users/" + userId);
                commit("setUser", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getUsersByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Users/GetByCompanyId/" + companyId);
                commit("setUsersByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        createNewUser(context, user) {
            return new Promise((resolve, reject) => {
                console.log(user);
                user.password = "";

                axios
                    .post("api/Auth/register", user)
                    .then((response) => {
                        context.commit("setUsers", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async updateUser({ dispatch }, userData) {
            try {
                await axios.put("api/Users/update", userData);
                await dispatch("getUser", userData.id);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteUser({ dispatch }, userId) {
            try {
                await axios.delete("api/Users/Delete?id=" + userId);
                await dispatch("getAllUsers");
            } catch (error) {
                console.log(error);
            }
        }

    }
};