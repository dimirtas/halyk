import axios from "../../axios/requestAxios";

export default 
{
    namespaced: true,
    state: {
        application: null,
        applications: null
    },
    getters: {
        application(state) {
        return state.application;
        },
        applications(state) {
        return state.applications;
        }
    },
    mutations: {
        setApplication(state, data) {
        state.application = data;
        },
        setApplications(state, data) {
        state.applications = data;
        }
    },
    actions: {
        getAllApplications(context) {
            return new Promise((resolve, reject) => {
                axios
                .get("api/Applications")
                .then((response) => {
                    context.commit("setApplications", response.data);
                    resolve(response);
                })
                .catch((error) => {
                    reject(error.response);
                });
            });
        },

        async getApplication({ commit }, applicationId) {
            try {
                const { data } = await axios.get("api/Applications/" + applicationId);
                console.log(data);
                commit("setApplication", data);
            } 
            catch (error) {
                console.log(error);
            }
        },
        
        async deleteApplication({ dispatch }, applicationId) {
            try {
                await axios.delete("api/Applications?id=" + applicationId);
                await dispatch("getAllApplications");}
            catch (error) {
            console.log(error);
            }
        },

        async updateApplication({ dispatch }, application) {
            try {
                await axios.put("api/Applications", application);
                await dispatch("getApplication", application.id);
            }
            catch (error) {
                console.log(error);
            }
        }, 

        async createApplication({ dispatch }, application) {
            try {
                console.log(application)
                await axios.post("api/Applications", application);          
                await dispatch("getAllApplications");
            }
            catch (error) {
                console.log(error);
            }
        }
    }
};
