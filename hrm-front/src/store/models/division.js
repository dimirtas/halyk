import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        division: null,
        divisions: null,
        divisionsByCompanyId: null
    },
    getters: {
        division(state) {
            return state.division;
        },
        divisions(state) {
            return state.divisions;
        },
        divisionsByCompanyId(state) {
            return state.divisionsByCompanyId;
        }
    },
    mutations: {
        setDivision(state, data) {
            state.division = data;
        },
        setDivisions(state, data) {
            state.divisions = data;
        },
        setDivisionsByCompanyId(state, data) {
            state.divisionsByCompanyId = data;
        }
    },
    actions: {
        getAllDivisions(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Divisions")
                    .then((response) => {
                        context.commit("setDivisions", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getDivision({ commit }, divisionId) {
            try {
                const { data } = await axios.get("api/Divisions/" + divisionId);
                console.log(data);
                commit("setDivision", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getDivisionsByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Divisions/GetByCompanyId/" + companyId);
                commit("setDivisionsByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteDivision({ dispatch }, divisionId) {
            try {
                await axios.delete("api/Divisions/Delete?id=" + divisionId);
                await dispatch("getAllDivisions");
            } catch (error) {
                console.log(error);
            }
        },

        async updateDivision({ dispatch }, division) {
            try {
                await axios.put("api/Divisions/update", division);
                await dispatch("getAllDivisions");
            } catch (error) {
                console.log(error);
            }
        },

        async createDivision({ dispatch }, division) {
            try {
                console.log(division)
                await axios.post("api/Divisions/create", division);
                await dispatch("getAllDivisions");
            } catch (error) {
                console.log(error);
            }
        }

    }
};