﻿import axios from "../../axios/requestAxios";

export default 
{
    namespaced: true,
    state: {
        vacancy: null,
        vacancies: null
    },
    getters: {
        vacancy(state) {
        return state.vacancy;
        },
        vacancies(state) {
        return state.vacancies;
        }
    },
    mutations: {
        setVacancy(state, data) {
        state.vacancy = data;
        },
        setVacancies(state, data) {
        state.vacancies = data;
        }
    },
    actions: {
        getAllVacancies(context) {
            return new Promise((resolve, reject) => {
                axios
                .get("api/Vacancies")
                .then((response) => {
                    context.commit("setVacancies", response.data);
                    resolve(response);
                })
                .catch((error) => {
                    reject(error.response);
                });
            });
        },

        async getVacancy({ commit }, vacancyId) {
            try {
                const { data } = await axios.get("api/Vacancies/" + vacancyId);
                console.log(data);
                commit("setVacancy", data);
            } 
            catch (error) {
                console.log(error);
            }
        },
        
        async deleteVacancy({ dispatch }, vacancyId) {
            try {
                await axios.delete("api/Vacancies/Delete?id=" + vacancyId);
                await dispatch("getAllVacancies");}
            catch (error) {
            console.log(error);
            }
        },

        async updateVacancy({ dispatch }, vacancy) {
            try {
                await axios.put("api/Vacancies/update", vacancy);
                await dispatch("getVacancies", vacancy.id);
            }
            catch (error) {
                console.log(error);
            }
        }, 

        async createVacancy({ dispatch }, vacancy) {
            try {
                console.log(vacancy)
                await axios.post("api/Vacancies/create", vacancy);          
                await dispatch("getAllVacancies");
            }
            catch (error) {
                console.log(error);
            }
        },
        
        async createVacancyFromApp({ commit }, application) {
            try {
                const { data } = await axios.post("api/Vacancies/createFromApp", application);
                commit("setVacancy", data);
            }
            catch (error) {
                console.log(error);
            }
        }
    }
};
