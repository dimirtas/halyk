﻿import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        knowledge: null,
        knowledges: null,
        knowledgesByCompanyId: null
    },
    getters: {
        knowledge(state) {
            return state.knowledge;
        },
        knowledges(state) {
            return state.knowledges;
        },
        knowledgesByCompanyId(state) {
            return state.knowledgesByCompanyId;
        }
    },
    mutations: {
        setKnowledge(state, data) {
            state.knowledge = data;
        },
        setKnowledges(state, data) {
            state.knowledges = data;
        },
        setKnowledgesByCompanyId(state, data) {
            state.knowledgesByCompanyId = data;
        }
    },
    actions: {
        getAllKnowledges(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Knowledges")
                    .then((response) => {
                        context.commit("setKnowledges", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getKnowledge({ commit }, knowledgeId) {
            try {
                const { data } = await axios.get("api/Knowledges/" + knowledgeId);
                console.log(data);
                commit("setKnowledge", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getKnowledgesByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Knowledges/GetByCompanyId/" + companyId);
                commit("setKnowledgesByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteKnowledge({ dispatch }, knowledgeId) {
            try {
                await axios.delete("api/Knowledges/Delete?id=" + knowledgeId);
                await dispatch("getAllKnowledges");
            } catch (error) {
                console.log(error);
            }
        },

        async updateKnowledge({ dispatch }, knowledge) {
            try {
                await axios.put("api/Knowledges/update", knowledge);
                await dispatch("getAllKnowledges");
            } catch (error) {
                console.log(error);
            }
        },

        async createKnowledge({ dispatch }, knowledge) {
            try {
                console.log(knowledge)
                await axios.post("api/Knowledges/create", knowledge);
                await dispatch("getAllKnowledges");
            } catch (error) {
                console.log(error);
            }
        }

    }
};