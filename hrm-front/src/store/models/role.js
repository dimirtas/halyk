import axios from "../../axios/requestAxios";

export default {
  namespaced: true,
  state: {
    role: null,
    roles: null
  },
  getters: {
    role(state) {
      return state.role;
    },
    roles(state) {
      return state.roles;
    }
  },
  mutations: {
    setRole(state, data) {
      state.role = data;
    },
    setRoles(state, data) {
      state.roles = data;
    }
  },
  actions: {
    getAllRoles(context) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/Roles")
          .then((response) => {
            context.commit("setRoles", response.data);
            resolve(response);
          })
          .catch((error) => {
            reject(error.response);
          });
      });
    },

    async getRole({ commit }, roleId) {
      try {
        const { data } = await axios.get("api/Roles/" + roleId);
        console.log(data);
        commit("setRole", data);
      } catch (error) {
        console.log(error);
      }
    },
    
    async deleteRole({ dispatch }, roleId) {
      try {
          await axios.delete("api/Roles/Delete?id=" + roleId);
          await dispatch("getAllRoles");
      } catch (error) {
          console.log(error);
      }
    },

    async updateRole({ dispatch }, role) {
      try {
          await axios.put("api/Roles/update", role);
          await dispatch("getAllRoles");
      } catch (error) {
          console.log(error);
      }
    }, 

    async createRole({ dispatch }, role) {
      try {
        console.log(role)
          await axios.post("api/Roles/create", role);          
          await dispatch("getAllRoles");
      } catch (error) {
          console.log(error);
      }
    }

  }
};
