import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        ability: null,
        abilities: null,
        abilitiesByCompanyId: null
    },
    getters: {
        ability(state) {
            return state.ability;
        },
        abilities(state) {
            return state.abilities;
        },
        abilitiesByCompanyId(state) {
            return state.abilitiesByCompanyId;
        }
    },
    mutations: {
        setAbility(state, data) {
            state.ability = data;
        },
        setAbilities(state, data) {
            state.abilities = data;
        },
        setAbilitiesByCompanyId(state, data) {
            state.abilitiesByCompanyId = data;
        }
    },
    actions: {
        getAllAbilities(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Abilities")
                    .then((response) => {
                        context.commit("setAbilities", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getAbility({ commit }, abilityId) {
            try {
                const { data } = await axios.get("api/Abilities/" + abilityId);
                console.log(data);
                commit("setAbility", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getAbilitiesByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Abilities/GetByCompanyId/" + companyId);
                commit("setAbilitiesByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteAbility({ dispatch }, abilityId) {
            try {
                await axios.delete("api/Abilities/Delete?id=" + abilityId);
                await dispatch("getAllAbilities");
            } catch (error) {
                console.log(error);
            }
        },

        async updateAbility({ dispatch }, ability) {
            try {
                await axios.put("api/Abilities/update", ability);
                await dispatch("getAllAbilities");
            } catch (error) {
                console.log(error);
            }
        },

        async createAbility({ dispatch }, ability) {
            try {
                console.log(ability)
                await axios.post("api/Abilities/create", ability);
                await dispatch("getAllAbilities");
            } catch (error) {
                console.log(error);
            }
        }

    }
};