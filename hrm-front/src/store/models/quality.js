﻿import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        quality: null,
        qualities: null,
        qualitiesByCompanyId: null
    },
    getters: {
        quality(state) {
            return state.quality;
        },
        qualities(state) {
            return state.qualities;
        },
        qualitiesByCompanyId(state) {
            return state.qualitiesByCompanyId;
        }
    },
    mutations: {
        setQuality(state, data) {
            state.quality = data;
        },
        setQualities(state, data) {
            state.qualities = data;
        },
        setQualitiesByCompanyId(state, data) {
            state.qualitiesByCompanyId = data;
        }
    },
    actions: {
        getAllQualities(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Qualities")
                    .then((response) => {
                        context.commit("setQualities", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getQuality({ commit }, qualityId) {
            try {
                const { data } = await axios.get("api/Qualities/" + qualityId);
                console.log(data);
                commit("setQuality", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getQualitiesByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Qualities/GetByCompanyId/" + companyId);
                commit("setQualitiesByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteQuality({ dispatch }, qualityId) {
            try {
                await axios.delete("api/Qualities/Delete?id=" + qualityId);
                await dispatch("getAllQualities");
            } catch (error) {
                console.log(error);
            }
        },

        async updateQuality({ dispatch }, quality) {
            try {
                await axios.put("api/Qualities/update", quality);
                await dispatch("getAllQualities");
            } catch (error) {
                console.log(error);
            }
        },

        async createQuality({ dispatch }, quality) {
            try {
                console.log(quality)
                await axios.post("api/Qualities/create", quality);
                await dispatch("getAllQualities");
            } catch (error) {
                console.log(error);
            }
        }

    }
};