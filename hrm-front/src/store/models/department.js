import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        department: null,
        departments: null,
        departmentsByCompanyId: null
    },
    getters: {
        department(state) {
            return state.department;
        },
        departments(state) {
            return state.departments;
        },
        departmentsByCompanyId(state) {
            return state.departmentsByCompanyId;
        }
    },
    mutations: {
        setDepartment(state, data) {
            state.department = data;
        },
        setDepartments(state, data) {
            state.departments = data;
        },
        setDepartmentsByCompanyId(state, data) {
            state.departmentsByCompanyId = data;
        }
    },
    actions: {
        getAllDepartments(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Departments")
                    .then((response) => {
                        context.commit("setDepartments", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getDepartment({ commit }, departmentId) {
            try {
                const { data } = await axios.get("api/Departments/" + departmentId);
                console.log(data);
                commit("setDepartment", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getDepartmentsByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Departments/GetByCompanyId/" + companyId);
                commit("setDepartmentsByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteDepartment({ dispatch }, departmentId) {
            try {
                await axios.delete("api/Departments/Delete?id=" + departmentId);
                await dispatch("getAllDepartments");
            } catch (error) {
                console.log(error);
            }
        },

        async updateDepartment({ dispatch }, department) {
            try {
                await axios.put("api/Departments/update", department);
                await dispatch("getAllDepartments");
            } catch (error) {
                console.log(error);
            }
        },

        async createDepartment({ dispatch }, department) {
            try {
                console.log(department)
                await axios.post("api/Departments/create", department);
                await dispatch("getAllDepartments");
            } catch (error) {
                console.log(error);
            }
        }

    }
};