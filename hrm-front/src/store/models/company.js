import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        company: null,
        companies: null
    },
    getters: {
        company(state) {
            return state.company;
        },
        companies(state) {
            return state.companies;
        }
    },
    mutations: {
        setCompany(state, data) {
            state.company = data;
        },
        setCompanies(state, data) {
            state.companies = data;
        }
    },
    actions: {
        getAllCompanies(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Companies")
                    .then((response) => {
                        context.commit("setCompanies", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },
        
    async getCompany({ commit }, companyId) {
        try {
          const { data } = await axios.get("api/Companies/" + companyId);
          console.log(data);
          commit("setCompany", data);
        } catch (error) {
          console.log(error);
        }
      },
      
      async deleteCompany({ dispatch }, companyId) {
        try {
            await axios.delete("api/Companies/Delete?id=" + companyId);
            await dispatch("getAllCompanies");
        } catch (error) {
            console.log(error);
        }
      },
  
      async updateCompany({ dispatch }, company) {
        try {
            await axios.put("api/Companies/update", company);
            await dispatch("getAllCompanies");
        } catch (error) {
            console.log(error);
        }
      }, 
  
      async createCompany({ dispatch }, company) {
        try {
          console.log(company)
            await axios.post("api/Companies/create", company);          
            await dispatch("getAllCompanies");
        } catch (error) {
            console.log(error);
        }
      }
  
    }
};