import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        resume: null,
        resumes: null
    },
    getters: {
        resume(state) {
            return state.resume;
        },
        resumes(state) {
            return state.resumes;
        }
    },
    mutations: {
        setResume(state, data) {
            state.resume = data;
        },
        setResumes(state, data) {
            state.resumes = data;
        }
    },
    actions: {
        async getAllResumes(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Resumes")
                    .then((response) => {
                        context.commit("setResumes", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getNewMessages({ dispatch }, id) {
            try {
                await axios.post("api/Resumes/GetMessages?id=" + id);
                await dispatch("getAllResumes");
            } catch (error) {
                console.log(error);
            }
        },

        async getResume({ commit }, resumeId) {
            try {
                const { data } = await axios.get("api/Resumes/" + resumeId);
                commit("setResume", data);
            } catch (error) {
                console.log(error);
            }
        },

        async createNewResume(context, resume) {
            console.log(resume)
            return new Promise((resolve, reject) => {
                axios
                    .post("api/Resumes/create", resume)
                    .then((response) => {
                        context.commit("setResumes", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async updateResume({ dispatch }, resumeData) {
            try {
                await axios.put("api/Resumes/update", resumeData);
                await dispatch("getResume", resumeData.id);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteResume({ dispatch }, resumeId) {
            try {
                await axios.delete("api/Resumes/Delete?id=" + resumeId);
                await dispatch("getAllResumes");
            } catch (error) {
                console.log(error);
            }
        }

    }
};