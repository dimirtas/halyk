import axios from "../../axios/requestAxios";

export default {
    namespaced: true,
    state: {
        city: null,
        cities: null,
        citiesByCompanyId: null
    },
    getters: {
        city(state) {
            return state.city;
        },
        cities(state) {
            return state.cities;
        },
        citiesByCompanyId(state) {
            return state.citiesByCompanyId;
        }
    },
    mutations: {
        setCity(state, data) {
            state.city = data;
        },
        setCities(state, data) {
            state.cities = data;
        },
        setCitiesByCompanyId(state, data) {
            state.citiesByCompanyId = data;
        }
    },
    actions: {
        getAllCities(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("api/Cities")
                    .then((response) => {
                        context.commit("setCities", response.data);
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error.response)
                    });
            });
        },

        async getCity({ commit }, cityId) {
            try {
                const { data } = await axios.get("api/Cities/" + cityId);
                console.log(data);
                commit("setCity", data);
            } catch (error) {
                console.log(error);
            }
        },

        async getCitiesByCompanyId({ commit }, companyId) {
            try {
                const { data } = await axios.get("api/Cities/GetByCompanyId/" + companyId);
                console.log(data);
                commit("setCitiesByCompanyId", data);
            } catch (error) {
                console.log(error);
            }
        },

        async deleteCity({ dispatch }, cityId) {
            try {
                await axios.delete("api/Cities/Delete?id=" + cityId);
                await dispatch("getAllCities");
            } catch (error) {
                console.log(error);
            }
        },

        async updateCity({ dispatch }, city) {
            try {
                await axios.put("api/Cities/update", city);
                await dispatch("getAllCities");
            } catch (error) {
                console.log(error);
            }
        },

        async createCity({ dispatch }, city) {
            try {
                console.log(city)
                await axios.post("api/Cities/create", city);
                await dispatch("getAllCities");
            } catch (error) {
                console.log(error);
            }
        }

    }
};