import axios from "axios";

const requestAxios = axios.create({
  baseURL: "http://localhost:5108/"
});

export default requestAxios;