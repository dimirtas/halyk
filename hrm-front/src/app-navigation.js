export default [{
        text: "Главная",
        path: "/home",
        icon: "home"
    },
    {
        text: "Администрирование",
        icon: "folder",
        items: [{
                text: "Пользователи",
                path: "/users"
            },
            {
                text: "Роли",
                path: "/roles"
            },
            {
                text: "Компании",
                path: "/companies",
                items: [{
                        text: "Города",
                        path: "/cities"
                    },
                    {
                        text: "Льготы",
                        path: "/benefits"
                    },
                    {
                        text: "Отделы",
                        path: "/departments"
                    },
                    {
                        text: "Департаменты",
                        path: "/divisions"
                    },
                    {
                        text: "Способности",
                        path: "/abilities"
                    },
                    {
                        text: "Личные характеристики",
                        path: "/qualities"
                    },
                    {
                        text: "Профессиональные знания",
                        path: "/knowledges"
                    },
                    {
                        text: "Компетенции",
                        path: "/skills"
                    },
                ]
            },
            {
                text: "Заявки",
                path: "/applications"
            },
            {
                text: "Вакансии",
                path: "/vacancies"
            },
            {
                text: "Резюме",
                path: "/resumes"
            }
        ]
    }
];