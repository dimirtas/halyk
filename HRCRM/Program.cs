using System.Reflection;
using System.Text.Json.Serialization;
using HRCRM.Config;
using HRCRM.Extensions;
using HRCRM.Models.Data;
using HRCRM.Services.LoggerService;
using HRCRM.Services.UserServices;
using LoggerService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NLog.Web;




    var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
    var connection = builder.Configuration.GetConnectionString("DefaultConnection");
    builder.Services.AddDbContext<CrmContext>(options => options.UseNpgsql(connection));

    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                // указывает, будет ли валидироваться издатель при валидации токена
                ValidateIssuer = true,
                // строка, представляющая издателя
                ValidIssuer = AuthOptions.Issuer,
                // будет ли валидироваться потребитель токена
                ValidateAudience = true,
                // установка потребителя токена
                ValidAudience = AuthOptions.Audience,
                // будет ли валидироваться время существования
                ValidateLifetime = true,
                // установка ключа безопасности
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                // валидация ключа безопасности
                ValidateIssuerSigningKey = true,
            };
        });

    builder.Services.AddCors(o => { o.AddDefaultPolicy(b => b.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()); });


    builder.Services.AddControllers().AddJsonOptions(x =>
        x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
    
    // Nlog
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();
    
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddServices(builder.Configuration);

    builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());

    var app = builder.Build();
    
    //Global logger
    var logger = app.Services.GetService<ILoggerManager>();
    app.ConfigureExceptionHandler(logger);

    using var scope = app.Services.CreateScope();
    var services = scope.ServiceProvider;
    try
    {
        var crmContext = services.GetRequiredService<CrmContext>();
        await AdminInitializer.SeedAdminUser(crmContext);
    }
    catch (Exception e)
    {
        var loggerAdmin = services.GetRequiredService<ILogger<Program>>();
        loggerAdmin.LogError(e, "Не удалось добавить админа");
    }


// Configure the HTTP request pipeline.
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("./swagger/v1/swagger.json", "HRMApp");
        options.RoutePrefix = String.Empty;
    });


    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseCors();

    app.UseAuthentication();
    app.UseAuthorization();
    app.UseEndpoints(options => { options.MapDefaultControllerRoute(); });

    app.MapControllers();

    app.Run();
