﻿using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.CityServices;

public interface ICityService
{
    public Task<IEnumerable<City>> GetAll();
    public Task<City?> GetById(int cityId);
    public Task Create (CityCreateViewModel model);
    public Task<bool> Delete (int cityId);
    public Task<bool> Update (CityViewModel model);
    public Task<bool> IsExists(string cityName);
    public Task<IEnumerable<City>> GetByCompanyId (int companyId);
}