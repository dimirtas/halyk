﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CityServices;

public class CityService : ICityService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;
    private readonly ICompanyService _companyService;

    public CityService(CrmContext db, IMapper mapper, ICompanyService companyService)
    {
        _db = db;
        _mapper = mapper;
        _companyService = companyService;
    }

   
    public async Task<IEnumerable<City>> GetAll()
    {
        return await _db.Cities
            .Where(c => c.IsDeleted == false)
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .ToListAsync();
    }
    
    public async Task<City?> GetById(int cityId)
    {
        return await _db.Cities
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .FirstOrDefaultAsync(c => c.Id == cityId && c.IsDeleted == false);
    }
    
    public async Task<IEnumerable<City>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Cities.Where(c => !c.IsDeleted);
        return null;
    }

    public async Task Create(CityCreateViewModel model)
    {
        var city = _mapper.Map<CityCreateViewModel, City>(model);
        _db.Cities.Add(city);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int cityId)
    {
        var city = await GetById(cityId);
        
        if (city != null)
        {
            city.IsDeleted = true;

            _db.Cities.Update(city);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> Update(CityViewModel model)
    {
        var city = await GetById(model.Id);

        if (city != null)
        {
            city.Name = model.Name;
            
            _db.Cities.Update(city);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> IsExists(string cityName)
    {
        return await _db.Cities
            .AnyAsync(r => r.Name == cityName && r.IsDeleted == false);
    }
}