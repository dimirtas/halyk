using HRCRM.Models;


namespace HRCRM.Services.KnowledgeServices;

public interface IKnowledgeService
{
    public Task<IEnumerable<Knowledge>> GetAll();
    public Task<Knowledge?> GetById(int id);
    public Task Create (Knowledge model);
    public Task Delete (Knowledge model);
    public Task<bool> Update (Knowledge model);
    public Task<bool> IsExists(string name);
    public Task<IEnumerable<Knowledge>> GetByCompanyId (int companyId);
}