using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;


namespace HRCRM.Services.KnowledgeServices;

public class KnowledgeService : IKnowledgeService
{
    private readonly CrmContext _db;
    private readonly ICompanyService _companyService;

    public KnowledgeService(CrmContext db, ICompanyService companyService)
    {
        _db = db;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Knowledge>> GetAll()
    {
        return await _db.Knowledges
            .Where(a => !a.IsDeleted)
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .ToListAsync();
    }

    public async Task<Knowledge?> GetById(int id)
    {
        return await _db.Knowledges
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task<IEnumerable<Knowledge>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Knowledges.Where(k => !k.IsDeleted);
        return null;
    }
   
    public async Task Create(Knowledge model)
    {
        await _db.Knowledges.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Knowledge model)
    {
        model.IsDeleted = true;
        _db.Knowledges.Update(model);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Update(Knowledge model)
    {
        var knowledge = await GetById(model.Id);

        if (knowledge != null)
        {
            knowledge.Name = model.Name;
            
            _db.Knowledges.Update(knowledge);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> IsExists(string name)
    {
        return await _db.Knowledges
            .AnyAsync(k => k.Name == name && k.IsDeleted == false);
    }
}