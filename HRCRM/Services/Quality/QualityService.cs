using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;


namespace HRCRM.Services.QualityServices;

public class QualityService : IQualityService
{
    private readonly CrmContext _db;
    private readonly ICompanyService _companyService;


    public QualityService(CrmContext db, ICompanyService companyService)
    {
        _db = db;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Quality>> GetAll()
    {
        return await _db.Qualities
            .Where(a => !a.IsDeleted)
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .ToListAsync();
    }

    public async Task<Quality?> GetById(int id)
    {
        return await _db.Qualities
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task<IEnumerable<Quality>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Qualities.Where(k => !k.IsDeleted);
        return null;
    }
   
    public async Task Create(Quality model)
    {
        await _db.Qualities.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Quality model)
    {
        model.IsDeleted = true;
        _db.Qualities.Update(model);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Update(Quality model)
    {
        var quality = await GetById(model.Id);

        if (quality != null)
        {
            quality.Name = model.Name;
            
            _db.Qualities.Update(quality);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> IsExists(string name)
    {
        return await _db.Qualities
            .AnyAsync(k => k.Name == name && k.IsDeleted == false);
    }
}