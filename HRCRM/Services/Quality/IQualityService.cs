using HRCRM.Models;


namespace HRCRM.Services.QualityServices;

public interface IQualityService
{
    public Task<IEnumerable<Quality>> GetAll();
    public Task<Quality?> GetById(int id);
    public Task Create (Quality model);
    public Task Delete (Quality model);
    public Task<bool> Update (Quality model);
    public Task<bool> IsExists(string name);
    public Task<IEnumerable<Quality>> GetByCompanyId (int companyId);
}