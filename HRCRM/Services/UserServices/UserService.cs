using System.Security.Claims;
using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.User;
using HRCRM.Services.CompanyServices;
using HRCRM.Services.EmailServices;
using HRCRM.Services.PasswordServices;
using HRCRM.Services.RoleServices;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.UserServices;

public class UserService : IUserService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;
    private readonly IRoleService _roleService;
    private readonly ICompanyService _companyService;
    private readonly IPasswordService _passwordService;
    private readonly IEmailService _emailService;
    
    public UserService(CrmContext db, IMapper mapper, IRoleService roleService, ICompanyService companyService, IPasswordService passwordService, IEmailService emailService)
    {
        _db = db;
        _mapper = mapper;
        _roleService = roleService;
        _companyService = companyService;
        _passwordService = passwordService;
        _emailService = emailService;
    }

    public async Task<IEnumerable<User>> GetAll()
    {
        return await _db.Users.Include(u => u.Company).Where(u => !u.IsDeleted).ToListAsync();
    }
    
    public async Task<User?> FindById(int userId)
    {
        return await _db.Users
            .Include(u => u.Roles).Include(u => u.Company).FirstOrDefaultAsync(u => u.Id == userId && !u.IsDeleted);
    }
    
    public async Task<User?> FindByEmail(string email)
    {
        return await _db.Users
            .Include(u => u.Roles).Include(u => u.Company).FirstOrDefaultAsync(u => u.Email == email && !u.IsDeleted);
    }

    public async Task<User?> FindByRefreshToken(string refreshToken)
    {
        return await _db.Users
            .Include(u => u.Roles)
            .FirstOrDefaultAsync(u => u.RefreshToken == refreshToken);
    }

    public ClaimsIdentity GetIdentity(User user)
    {
        var claims = new List<Claim> { new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username) };

        foreach (var role in user.Roles)
            claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role.Name));

        ClaimsIdentity claimsIdentity = new ClaimsIdentity(
            claims: claims, 
            "Token",
            ClaimsIdentity.DefaultNameClaimType,
            ClaimsIdentity.DefaultRoleClaimType);
        
        return claimsIdentity;
    }

    public async Task<bool> IsEmailExists(string email)
    {
        return await _db.Users.AnyAsync(u => u.Email == email && !u.IsDeleted);
    }
    
    public async Task<bool> IsPhoneNumberExists(string phone)
    {
        return await _db.Users.AnyAsync(u => u.Phone == phone && !u.IsDeleted);
    }
    
    public async Task<bool> Update(UserModelRequest model)
    {
        var user = await FindById(model.Id);

        if (user != null)
        {
            List<Role> roles = new List<Role>();
            
            foreach (var role in model.RoleIds)
            {
                var result = await _roleService.GetById(role);
                if (result != null)
                    roles.Add(result);
            }
            
            user.Roles = roles;
            user.Username = model.Email;
            _mapper.Map(model, user);
            _db.Users.Update(user);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> Delete(int userId)
    {
        var user = await FindById(userId);

        if (user != null)
        {
            user.IsDeleted = true;
            _db.Users.Update(user);
            await _db.SaveChangesAsync();
            return true;
        }

        return false;
    }
    
    public async Task<IEnumerable<User>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Users.Where(c => !c.IsDeleted);
        return null;
    }

    public async Task<bool> ChangePassword(User user, string email, string checkingCurrentPassword, string? newPassword)
    {
        if (!_passwordService.VerifyHashedPassword(user.Password, checkingCurrentPassword))
            return false;
        
        user.Password = _passwordService.CreatePasswordHash(newPassword);
        _db.Users.Update(user);
        await _db.SaveChangesAsync();

        return true;
    }

    public async Task RemindPassword(User user, string email)
    {
        string newPassword = _passwordService.GenerateNewPassword(12);
        user.Password = _passwordService.CreatePasswordHash(newPassword);

        _db.Update(user);
        await _db.SaveChangesAsync();

        await _emailService.SendEmailAsync(email, "Восстановление пароля", $"Ваш новый пароль {newPassword}");
    }
}