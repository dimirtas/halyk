using System.Security.Claims;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.User;

namespace HRCRM.Services.UserServices;

public interface IUserService
{
    public Task<IEnumerable<User>> GetAll();
    public Task<User?> FindById(int userId);
    public Task<User?> FindByEmail(string email);
    public Task<User?> FindByRefreshToken(string refreshToken);
    public ClaimsIdentity GetIdentity(User user);
    public Task<bool> IsEmailExists(string email);
    public Task<bool> IsPhoneNumberExists(string email);
    public Task<bool> Update (UserModelRequest model);
    public Task<bool> Delete(int userId);
    public Task<IEnumerable<User>> GetByCompanyId (int companyId);
    public Task<bool> ChangePassword(User user, string email, string checkingCurrentPassword, string? newPassword);
    public Task RemindPassword(User user, string email);
}