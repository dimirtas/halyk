﻿using HRCRM.Models;
using HRCRM.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.UserServices;

public class AdminInitializer
{
    public static async Task SeedAdminUser(CrmContext db)
    {
        var admin = new User()
        {
            Username = "admin@admin.com",
            Password = "AICKOIMpm5WdBGchKv0q2mVoX/OJLoh4GswxdklziPc1cfSS2jH0qSC5KcZ0z1BGNQ==",//!Qwerty1234
            Email = "admin@admin.com",
            FirstName = "Админ",
            SecondName = "Главный",
            Position = "Администратор системы",
            Phone = "87775345544"
        };
        
        var user = await db.Users.FirstOrDefaultAsync(a => a.Email == admin.Email);
        if (user == null)
        {
            await db.Users.AddAsync(admin);
            await db.SaveChangesAsync();
            var role = await db.Roles.FirstOrDefaultAsync(r => r.Name == "admin");
            if (role != null)
            {
                role.Users.Add(admin);
                admin.Roles.Add(role);
            }
        }
    
        await db.SaveChangesAsync();
    }
}