using HRCRM.Models;


namespace HRCRM.Services.AbilityServices;

public interface IAbilityService
{
    public Task<IEnumerable<Ability>> GetAll();
    public Task<Ability?> GetById(int id);
    public Task Create (Ability model);
    public Task Delete (Ability model);
    public Task<bool> Update (Ability model);
    public Task<bool> IsExists(string name);
    public Task<IEnumerable<Ability>> GetByCompanyId (int companyId);
}