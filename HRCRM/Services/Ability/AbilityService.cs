using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;


namespace HRCRM.Services.AbilityServices;

public class AbilityService : IAbilityService
{
    private readonly CrmContext _db;
    private readonly ICompanyService _companyService;


    public AbilityService(CrmContext db, ICompanyService companyService)
    {
        _db = db;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Ability>> GetAll()
    {
        return await _db.Abilities
            .Where(a => !a.IsDeleted)
            .Include(a => a.Vacancies)
            .Include(a => a.Companies)
            .ToListAsync();
    }

    public async Task<Ability?> GetById(int id)
    {
        return await _db.Abilities
            .Include(a => a.Vacancies)
            .Include(a => a.Companies)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task<IEnumerable<Ability>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Abilities.Where(a => !a.IsDeleted);
        return null;
    }
   
    public async Task Create(Ability model)
    {
        await _db.Abilities.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Ability model)
    {
        model.IsDeleted = true;
        _db.Abilities.Update(model);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Update(Ability model)
    {
        var ability = await GetById(model.Id);

        if (ability != null)
        {
            ability.Name = model.Name;
            
            _db.Abilities.Update(ability);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> IsExists(string name)
    {
        return await _db.Abilities
            .AnyAsync(k => k.Name == name && k.IsDeleted == false);
    }
}