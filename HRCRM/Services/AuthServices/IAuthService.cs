using System.Security.Claims;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.Auth;

namespace HRCRM.Services.AuthServices;

public interface IAuthService
{
    public Task<LoginModelResponse?> Login(LoginViewModel model);
    public Task Register(RegisterViewModel model);
    public Task<bool> Logout(int id);
}