using AutoMapper;
using HRCRM.Config;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.Auth;
using HRCRM.Models.ViewModels.User;
using HRCRM.Services.EmailServices;
using HRCRM.Services.PasswordServices;
using HRCRM.Services.RoleServices;
using HRCRM.Services.TokenServices;
using HRCRM.Services.UserServices;

namespace HRCRM.Services.AuthServices;

public class AuthService : IAuthService
{
    private readonly ITokenService _tokenService;
    private readonly IUserService _userService;
    private readonly IPasswordService _passwordService;
    private readonly IEmailService _emailService;
    private readonly CrmContext _db;
    private readonly IMapper _mapper;


    public AuthService(ITokenService tokenService, IUserService userService, IPasswordService passwordService, IEmailService emailService, CrmContext db, IMapper mapper)
    {
        _tokenService = tokenService;
        _userService = userService;
        _passwordService = passwordService;
        _emailService = emailService;
        _db = db;
        _mapper = mapper;
    }

    public async Task<LoginModelResponse?> Login(LoginViewModel model)
    {
        var user = await _userService.FindByEmail(model.Email);
        if (user == null || !_passwordService.VerifyHashedPassword(user.Password, model.Password) || user.IsDeleted)
            return null;

        var identity = _userService.GetIdentity(user);
        var encodeToken = _tokenService.CreateRefreshToken();
        
        user.RefreshToken = encodeToken;
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(AuthOptions.LifeTimeRefreshTokenInMinute);
        
        var response = new LoginModelResponse
        {
            User = _mapper.Map<UserModelResponse>(user),
            RefreshToken = encodeToken,
            AccessToken = _tokenService.CreateToken(identity),
            LifeTimeAccessToken = DateTime.Now.AddMinutes(AuthOptions.LifeTimeTokenInMinute),
        };

        _db.Users.Update(user);
        await _db.SaveChangesAsync();
        return response;
    }
    
    public async Task Register(RegisterViewModel model)
    {
        model.Password = _passwordService.GenerateNewPassword(12);
        
        var user = _mapper.Map<RegisterViewModel, User>(model);
        user.Password = _passwordService.CreatePasswordHash(model.Password);
        IQueryable<Role> roles = _db.Roles.Where(r => model.RoleIds.Contains(r.Id));
        user.Roles.AddRange(roles);
        _db.Users.Add(user);
        await _db.SaveChangesAsync();
        await _emailService.SendEmailAsync(user.Email, "Поздравляем с успешной регистрацией",
            $"Ваш логин: {user.Email}, Ваш пароль: {model.Password}");
    }
    
    public async Task<bool> Logout(int id)
    {
        var user = await _userService.FindById(id);
        if (user == null)
            return false;

        user.RefreshToken = null;
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(-1);
        
        _db.Users.Update(user);
        await _db.SaveChangesAsync();
        return true;
    }
}