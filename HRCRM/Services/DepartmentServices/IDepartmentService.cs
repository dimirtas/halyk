﻿using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.DepartmentServices;

public interface IDepartmentService
{
    public Task<IEnumerable<Department>> GetAll();
    public Task<Department?> GetById(int userId);
    public Task Create (DepartmentCreateViewModel model);
    public Task<bool> Delete (int departmentId);
    public Task<bool> Update (DepartmentViewModel model);
    public Task<bool> IsExists(string DepartmentName);
    public Task<IEnumerable<Department>> GetByCompanyId (int companyId);
}