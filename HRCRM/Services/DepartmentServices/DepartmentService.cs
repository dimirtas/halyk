﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using HRCRM.Services.DepartmentServices;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CityServices;

public class DepartmentService : IDepartmentService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;
    private readonly ICompanyService _companyService;


    public DepartmentService(CrmContext db, IMapper mapper, ICompanyService companyService)
    {
        _db = db;
        _mapper = mapper;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Department>> GetAll()
    {
        return await _db.Departments
            .Where(c => c.IsDeleted == false)
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .ToListAsync();
    }

    public async Task<Department?> GetById(int departmentId)
    {
        return await _db.Departments
            .Include(d => d.Companies)
            .Include(d => d.Applications)
            .FirstOrDefaultAsync(d => d.Id == departmentId && d.IsDeleted == false);
    }

    public async Task<IEnumerable<Department>> GetByCompanyId(int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Departments.Where(c => !c.IsDeleted);
        return null;
    }

    public async Task Create(DepartmentCreateViewModel model)
    {
        var department = _mapper.Map<DepartmentCreateViewModel, Department>(model);
        _db.Departments.Add(department);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int departmentId)
    {
        var department = await GetById(departmentId);

        if (department != null)
        {
            department.IsDeleted = true;

            _db.Departments.Update(department);
            await _db.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<bool> Update(DepartmentViewModel model)
    {
        var department = await GetById(model.Id);

        if (department != null)
        {
            department.Name = model.Name;

            _db.Departments.Update(department);
            await _db.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<bool> IsExists(string departmentName)
    {
        return await _db.Departments
            .AnyAsync(r => r.Name == departmentName && r.IsDeleted == false);
    }
}