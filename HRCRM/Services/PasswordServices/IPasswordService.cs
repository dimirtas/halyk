namespace HRCRM.Services.PasswordServices;

public interface IPasswordService
{ 
    public string CreatePasswordHash(string? password);
    public bool VerifyHashedPassword(string? hashedPassword, string password);
    public string GenerateNewPassword(int length);
}