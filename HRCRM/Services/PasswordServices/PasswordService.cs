using System.Security.Cryptography;
using System.Text;

namespace HRCRM.Services.PasswordServices;

public class PasswordService : IPasswordService
{
    public string CreatePasswordHash(string? password)
    {
        byte[] salt;
        byte[] buffer2;
        if (password == null)
            throw new ArgumentNullException(nameof(password));
        
        using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
        {
            salt = bytes.Salt;
            buffer2 = bytes.GetBytes(0x20);
        }
        byte[] dst = new byte[0x31];
        Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
        Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
        return Convert.ToBase64String(dst);
    }

    public bool VerifyHashedPassword(string? hashedPassword, string password)
    {
        byte[] buffer4;
        if (password == null)
            throw new ArgumentNullException(nameof(password));
        
        byte[] src = Convert.FromBase64String(hashedPassword);
        if (src.Length != 0x31 || src[0] != 0)
            return false;
        
        byte[] dst = new byte[0x10];
        Buffer.BlockCopy(src, 1, dst, 0, 0x10);
        byte[] buffer3 = new byte[0x20];
        Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
        using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8))
        {
            buffer4 = bytes.GetBytes(0x20);
        }

        return Convert.ToBase64String(buffer3) == Convert.ToBase64String(buffer4);
    }

    public string GenerateNewPassword(int length)
    {
        string upper = "QWERTYUIOPASDFGHJKLZXCVBNM";
        string lower = "qwertyuiopasdfghjklzxcvbnm";
        string nums = "1234567890";
        bool correctPassword = false;
        bool hasUpper = false;
        bool hasLower = false;
        bool hasDigit = false;
        Random rand = new Random();
        
        string password = string.Empty;
        
        while (!correctPassword)
        {
            password = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string symbols = upper + lower + nums;
                char c = symbols[rand.Next(0, symbols.Length)];
                password += c;
                if (upper.Contains(c)) hasUpper = true;
                if (lower.Contains(c)) hasLower = true;
                if (nums.Contains(c)) hasDigit = true;
            }

            correctPassword = hasDigit && hasLower && hasUpper;
        }

        return password;
    }
}