﻿using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels.Resume;
using OpenPop.Pop3;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace HRCRM.Services.EmailServices;

public class EmailService : IEmailService
{
    private readonly CrmContext _db;
    public EmailService(CrmContext db)
    {
        _db = db;
    }

    public async Task SendEmailAsync(string email, string subject, string message)
    {
        var emailMessage = new MimeMessage();
        emailMessage.From.Add(new MailboxAddress(
            "HRM By ESDP CSTeam2",
            "5a60k@mail.ru"
        ));
            
        emailMessage.To.Add(new MailboxAddress("", email));
        emailMessage.Subject = subject;
        emailMessage.Body = new TextPart(TextFormat.Html)
        {
            Text = message
        };

        using var client = new SmtpClient();
        await client.ConnectAsync("smtp.mail.ru", 25, false);
        await client.AuthenticateAsync("5a60k@mail.ru", "L8UZ5sKCs6j5X02aZXU1");
        await client.SendAsync(emailMessage);
        await client.DisconnectAsync(true);
    }
    
    public async Task GetNewMessages(int id)
    {
        Pop3Client client = new Pop3Client();
        client.Connect("pop.mail.ru", 995, true);
        client.Authenticate("nuraly_l@mail.ru", "SNLgevAfMQvKsq1Qgcpf");

        List<MessageUidModel> uidsModels = new List<MessageUidModel>();

        int messageCount = client.GetMessageCount();

        for (int i = 0; i < messageCount; i++)
        {
            uidsModels.Add(new MessageUidModel {Number = i + 1, Uid = client.GetMessageUid(i + 1)});
        }

        var existingUids = _db.Resumes.Where(r => uidsModels.Select(u => u.Uid).Contains(r.MessageUid)).Select(r => r.MessageUid).ToList();
            
        uidsModels.RemoveAll(u => existingUids.Contains(u.Uid));

        List<MessageUidModel> messages = new List<MessageUidModel>();

        foreach (var model in uidsModels)
        {
            MessageUidModel message = new MessageUidModel();
            message.Message = client.GetMessage(model.Number);
            message.Number = model.Number;
            message.Uid = client.GetMessageUid(model.Number);
            if (IsValid(message))
            {
                messages.Add(message);
            }
        }

        SaveNewMessages(messages, id);
    }
    
    public bool IsValid(MessageUidModel message)
    {
        var attachments = message.Message.FindAllAttachments();
        if (attachments.Count != 0 && message.Message.Headers.Subject.Contains("_"))
        {
            if (attachments[0].FileName.EndsWith(".docx") || attachments[0].FileName.EndsWith(".doc") ||
                attachments[0].FileName.EndsWith(".pdf"))
            {
                string[] splittedHeaders = message.Message.Headers.Subject.Split("_");
                if (splittedHeaders.Length == 3)
                {
                    return true;
                }
            }
        }

        return false;
    }
    
    public void SaveNewMessages(List<MessageUidModel> messages, int id)
    {
        List<Resume> resumes = new List<Resume>();
        foreach (var message in messages)
        {
            string[] splittedHeaders = message.Message.Headers.Subject.Split("_");
            Resume resume = new Resume();
            resume.Name = splittedHeaders[0];
            resume.Surname = splittedHeaders[1];
            resume.VacancyId = Convert.ToInt32(splittedHeaders[2]);
            resume.SenderEmail = message.Message.Headers.From.Address;
            resume.FileResume = Convert.ToBase64String(message.Message.MessagePart.MessageParts[1].Body);
            resume.FileName = message.Message.MessagePart.MessageParts[1].FileName;
            resume.CompanyId = id;
            resume.MessageUid = message.Uid;
            resumes.Add(resume);
        }

        _db.Resumes.AddRange(resumes);
        _db.SaveChanges();
    }
}