﻿namespace HRCRM.Services.EmailServices;

public interface IEmailService
{
    public Task SendEmailAsync(string email, string subject, string message);
    public Task GetNewMessages(int id);
}