using System.Security.Claims;

namespace HRCRM.Services.TokenServices;

public interface ITokenService
{
    public string CreateToken(ClaimsIdentity identity);
    public string CreateRefreshToken();
    public Task<object?> RefreshToken(string refreshToken);
}