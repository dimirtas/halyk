using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using HRCRM.Config;
using HRCRM.Models.Data;
using HRCRM.Services.UserServices;
using Microsoft.IdentityModel.Tokens;

namespace HRCRM.Services.TokenServices;

public class TokenService : ITokenService
{
    private readonly CrmContext _db;
    private readonly IUserService _userService;

    public TokenService(CrmContext db, IUserService userService)
    {
        _db = db;
        _userService = userService;
    }

    public string CreateToken(ClaimsIdentity identity)
    {
        var now = DateTime.UtcNow;
        var jwt = new JwtSecurityToken(
            AuthOptions.Issuer,
            AuthOptions.Audience,
            notBefore: now,
            claims: identity.Claims,
            signingCredentials: new SigningCredentials(
                AuthOptions.GetSymmetricSecurityKey(),
                SecurityAlgorithms.HmacSha256),
            expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LifeTimeTokenInMinute)));

        var encodeJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

        return encodeJwt;
    }

    public string CreateRefreshToken()
    {
        var randomNumber = new byte[64];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }

    public async Task<object?> RefreshToken(string refreshToken)
    {
        var user = await _userService.FindByRefreshToken(refreshToken);
        if (user == null || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
        {
            if (user != null) 
                user.RefreshToken = null;
            return null;
        }
        
        var identity = _userService.GetIdentity(user);
        var encodeToken = CreateRefreshToken();
        
        user.RefreshToken = encodeToken;
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(AuthOptions.LifeTimeRefreshTokenInMinute);
        
        var response = new
        {
            refreshToken = encodeToken,
            accessToken = CreateToken(identity),
            lifeTimeAccessToken = DateTime.UtcNow.AddMinutes(AuthOptions.LifeTimeTokenInMinute),
        };

        _db.Users.Update(user);
        await _db.SaveChangesAsync();
        return response;
    }
}