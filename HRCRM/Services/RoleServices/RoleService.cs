using System.Data;
using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.RoleServices;

public class RoleService : IRoleService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;

    public RoleService(CrmContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }

    public async Task<IEnumerable<Role>> GetAll()
    {
        return await _db.Roles
            .Include(r => r.Users)
            .Where(r => r.IsDeleted == false)
            .ToListAsync();
    }
    
    public async Task<Role?> GetById(int roleId)
    {
        return await _db.Roles
            .Include(r => r.Users)
            .FirstOrDefaultAsync(r => r.Id == roleId && r.IsDeleted == false);
    }
    
    public async Task Create(RoleNewViewModel model)
    {
        var role = _mapper.Map<RoleNewViewModel, Role>(model);
        _db.Roles.Add(role);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int roleId)
    {
        var role = await GetById(roleId);
        
        if (role != null)
        {
            role.IsDeleted = true;

            _db.Roles.Update(role);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> Update(RoleViewModel model)
    {
        var role = await GetById(model.Id);

        if (role != null)
        {
            role.Name = model.Name;
            
            _db.Roles.Update(role);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> IsExists(string roleName)
    {
        return await _db.Roles
            .AnyAsync(r => r.Name == roleName && r.IsDeleted == false);
    }
}