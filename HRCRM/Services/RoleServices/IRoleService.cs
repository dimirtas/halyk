using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.RoleServices;

public interface IRoleService 
{
    public Task<IEnumerable<Role>> GetAll();
    public Task<Role?> GetById(int roleId);
    public Task Create (RoleNewViewModel model);
    public Task<bool> Delete (int roleId);
    public Task<bool> Update (RoleViewModel model);
    public Task<bool> IsExists(string roleName);
}

