using HRCRM.Models;
using HRCRM.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.ApplicationServices;

public class ApplicationService : IApplicationService
{
    private readonly CrmContext _db;

    public ApplicationService(CrmContext db)
    {
        _db = db;
    }

    public async Task<IEnumerable<Application>> GetAll()
    {
        return await _db.Applications
            .Where(a => !a.IsDeleted)
            .Include(a => a.City)
            .Include(a => a.Department)
            .Include(a => a.Company)
            .Include(a => a.Division)
            .Include(a => a.LineManager)
            .Include(a => a.Benefits)
            .ToListAsync();
    }

    public async Task<Application?> GetById(int id)
    {
        return await _db.Applications
            .Include(a => a.City)
            .Include(a => a.Department)
            .Include(a => a.Company)
            .Include(a => a.Division)
            .Include(a => a.LineManager)
            .Include(a => a.Benefits)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }

    public async Task Create(Application model)
    {
        await _db.Applications.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Application model)
    {
        model.IsDeleted = true;
        await Update(model);
    }

    public async Task Update(Application model)
    {
        _db.Applications.Update(model);
        await _db.SaveChangesAsync();
    }
}