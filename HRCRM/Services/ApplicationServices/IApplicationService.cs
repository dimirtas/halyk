using HRCRM.Models;
using HRCRM.Models.ViewModels.Application;

namespace HRCRM.Services.ApplicationServices;

public interface IApplicationService
{
    public Task<IEnumerable<Application>> GetAll();
    public Task<Application?> GetById(int id);
    public Task Create (Application model);
    public Task Delete (Application model);
    public Task Update (Application model);
}