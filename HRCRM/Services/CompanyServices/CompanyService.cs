using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CompanyServices;

public class CompanyService : ICompanyService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;

    public CompanyService(CrmContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }

    public async Task<IEnumerable<Company>> GetAllCompanies()
    {
        return await _db.Companies
            .Include(c => c.Cities)
            .Include(c => c.Benefits)
            .Include(c => c.Departments)
            .Include(c => c.Divisions)
            .Include(c => c.Knowledges)
            .Include(c => c.Abilities)
            .Include(c => c.Qualities)
            .Include(c => c.Skills)
            .Where(c => c.IsDeleted == false)
            .ToListAsync();
    }

    public async Task<Company?> GetById(int companyId)
    {
        return await _db.Companies
            .Include(c => c.Cities)
            .Include(c => c.Benefits)
            .Include(c => c.Departments)
            .Include(c => c.Divisions)
            .Include(c => c.Users)
            .Include(c => c.Knowledges)
            .Include(c => c.Abilities)
            .Include(c => c.Qualities)
            .Include(c => c.Skills)
            .FirstOrDefaultAsync(c => !c.IsDeleted && c.Id == companyId);
    }

    public async Task Create(CompanyCreateViewModel model)
    {
        var company = _mapper.Map<CompanyCreateViewModel, Company>(model);
        _db.Companies.Add(company);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int companyId)
    {
        var company = await GetById(companyId);

        if (company != null)
        {
            company.IsDeleted = true;

            _db.Companies.Update(company);
            await _db.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<bool> Update(CompanyViewModel model)
    {
        var company = await GetById(model.Id);

        if (company != null)
        {
            company.Name = model.Name;
            
            _db.Companies.Update(company);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> IsExists(string companyBin)
    {
        return await _db.Companies
            .AnyAsync(c => c.Bin == companyBin && c.IsDeleted == false);
    }
    
}