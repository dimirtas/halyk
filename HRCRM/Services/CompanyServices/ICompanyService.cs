using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.CompanyServices;

public interface ICompanyService
{
    public Task<Company?> GetById(int companyId);
    public Task<IEnumerable<Company>> GetAllCompanies();
    public Task Create (CompanyCreateViewModel model);
    public Task<bool> Delete (int companyId);
    public Task<bool> Update (CompanyViewModel model);
    public Task<bool> IsExists(string companyName);
}