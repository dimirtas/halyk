using HRCRM.Models;


namespace HRCRM.Services.SkillServices;

public interface ISkillService
{
    public Task<IEnumerable<Skill>> GetAll();
    public Task<Skill?> GetById(int id);
    public Task Create (Skill model);
    public Task Delete (Skill model);
    public Task<bool> Update (Skill model);
    public Task<bool> IsExists(string name);
    public Task<IEnumerable<Skill>> GetByCompanyId (int companyId);
}