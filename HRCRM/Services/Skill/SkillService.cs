using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;


namespace HRCRM.Services.SkillServices;

public class SkillService : ISkillService
{
    private readonly CrmContext _db;
    private readonly ICompanyService _companyService;


    public SkillService(CrmContext db, ICompanyService companyService)
    {
        _db = db;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Skill>> GetAll()
    {
        return await _db.Skills
            .Where(a => !a.IsDeleted)
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .ToListAsync();
    }

    public async Task<Skill?> GetById(int id)
    {
        return await _db.Skills
            .Include(k => k.Vacancies)
            .Include(k => k.Companies)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task<IEnumerable<Skill>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Skills.Where(s => !s.IsDeleted);
        return null;
    }
   
    public async Task Create(Skill model)
    {
        await _db.Skills.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Skill model)
    {
        model.IsDeleted = true;
        _db.Skills.Update(model);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Update(Skill model)
    {
        var skill = await GetById(model.Id);

        if (skill != null)
        {
            skill.Name = model.Name;
            
            _db.Skills.Update(skill);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
    
    public async Task<bool> IsExists(string name)
    {
        return await _db.Skills
            .AnyAsync(k => k.Name == name && k.IsDeleted == false);
    }
}