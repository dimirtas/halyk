using HRCRM.Models;
using HRCRM.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.VacancyServices;

public class VacancyService : IVacancyService
{
    private readonly CrmContext _db;

    public VacancyService(CrmContext db)
    {
        _db = db;
    }

    public async Task<IEnumerable<Vacancy>> GetAll()
    {
        return await _db.Vacancies
            .Where(a => !a.IsDeleted)
            .Include(v => v.City)
            .Include(v => v.Department)
            .Include(v => v.Division)
            .Include(v => v.PositionDirectManager)
            .Include(v => v.ManagerOfTheManager)
            .Include(v => v.Company)
            .Include(v => v.Knowledges)
            .Include(v => v.Skills)
            .Include(v => v.Qualities)
            .Include(v => v.Benefits)
            .ToListAsync();
    }
 

    public async Task<Vacancy?> GetById(int id)
    {
        return await _db.Vacancies
            .Include(v => v.City)
            .Include(v => v.Department)
            .Include(v => v.Division)
            .Include(v => v.PositionDirectManager)
            .Include(v => v.ManagerOfTheManager)
            .Include(v => v.Company)
            .Include(v => v.Knowledges)
            .Include(v => v.Skills)
            .Include(v => v.Qualities)
            .Include(v => v.Benefits)
            .Where(a => !a.IsDeleted && a.Id == id).FirstOrDefaultAsync();
    }

    public async Task Create(Vacancy model)
    {
        await _db.Vacancies.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Vacancy model)
    {
        model.IsDeleted = true;
        await Update(model);
    }

    public async Task Update(Vacancy model)
    {
        _db.Vacancies.Update(model);
        await _db.SaveChangesAsync();
    }
}