using HRCRM.Models;
using HRCRM.Models.ViewModels.Application;

namespace HRCRM.Services.VacancyServices;

public interface IVacancyService
{
    public Task<IEnumerable<Vacancy>> GetAll();
    public Task<Vacancy?> GetById(int id);
    public Task Create (Vacancy model);
    public Task Delete (Vacancy model);
    public Task Update (Vacancy model);
}