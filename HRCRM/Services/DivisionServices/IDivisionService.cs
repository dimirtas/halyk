﻿using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.DivisionServices;

public interface IDivisionService
{
    public Task<IEnumerable<Division>> GetAll();
    public Task<Division?> GetById(int divisionId);
    public Task Create (DivisionCreateViewModel model);
    public Task<bool> Delete (int divisionId);
    public Task<bool> Update (DivisionViewModel model);
    public Task<bool> IsExists(string DivisionName);
    public Task<IEnumerable<Division>> GetByCompanyId (int companyId);
}