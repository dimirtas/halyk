﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.CompanyServices;
using HRCRM.Services.DivisionServices;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CityServices;

public class DivisionService : IDivisionService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;
    private readonly ICompanyService _companyService;


    public DivisionService(CrmContext db, IMapper mapper, ICompanyService companyService)
    {
        _db = db;
        _mapper = mapper;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Division>> GetAll()
    {
        return await _db.Divisions
            .Where(c => c.IsDeleted == false)
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .ToListAsync();
    }
    
    public async Task<Division?> GetById(int divisionId)
    {
        return await _db.Divisions
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .FirstOrDefaultAsync(c => c.Id == divisionId && c.IsDeleted == false);
    }
    
    public async Task<IEnumerable<Division>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Divisions.Where(c => !c.IsDeleted);
        return null;
    }

    public async Task Create(DivisionCreateViewModel model)
    {
        var division = _mapper.Map<DivisionCreateViewModel, Division>(model);
        _db.Divisions.Add(division);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int divisionId)
    {
        var division = await GetById(divisionId);
        
        if (division != null)
        {
            division.IsDeleted = true;

            _db.Divisions.Update(division);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> Update(DivisionViewModel model)
    {
        var division = await GetById(model.Id);

        if (division != null)
        {
            division.Name = model.Name;
            
            _db.Divisions.Update(division);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> IsExists(string divisionName)
    {
        return await _db.Benefits
            .AnyAsync(r => r.Name == divisionName && r.IsDeleted == false);
    }
}