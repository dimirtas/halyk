﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels.Resume;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.ResumeServices;

public class ResumeService : IResumeService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;

    public ResumeService(CrmContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }
    
    public async Task<IEnumerable<Resume>> GetAll()
    {
        return await _db.Resumes
            .Include(r => r.Vacancy)
            .Include(r => r.Company)
            .Where(r => r.IsDeleted == false)
            .ToListAsync();
    }

    public async Task<Resume?> GetById(int resumeId)
    {
        return await _db.Resumes
            .Include(r => r.Vacancy)
            .Include(r => r.Company)
            .FirstOrDefaultAsync(c => !c.IsDeleted && c.Id == resumeId);
    }

    public async Task Create(ResumeModelRequest resumeModel)
    {
        var resume = _mapper.Map<ResumeModelRequest, Resume>(resumeModel);
        _db.Resumes.Add(resume);
        await _db.SaveChangesAsync();
    }
    
    public async Task<bool> Update(ResumeModelRequest resumeModel)
    {
        var resume = await GetById(resumeModel.Id);

        if (resume != null)
        {
            _mapper.Map(resumeModel, resume);
            
            _db.Resumes.Update(resume);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;  
    }

    public async Task<bool> Delete(int resumeId)
    {
        var resume = await GetById(resumeId);
        
        if (resume != null)
        {
            resume.IsDeleted = true;

            _db.Resumes.Update(resume);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }
}