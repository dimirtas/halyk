﻿using HRCRM.Models;
using HRCRM.Models.ViewModels.Resume;

namespace HRCRM.Services.ResumeServices;

public interface IResumeService
{
    public Task<IEnumerable<Resume>> GetAll();
    public Task<Resume?> GetById(int resumeId);
    public Task Create (ResumeModelRequest resumeModel);
    public Task<bool> Update (ResumeModelRequest resumeModel);
    public Task<bool> Delete (int resumeId);
}