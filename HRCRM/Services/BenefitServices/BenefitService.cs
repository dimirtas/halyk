﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.Data;
using HRCRM.Models.ViewModels;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.CompanyServices;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CityServices;

public class BenefitService : IBenefitService
{
    private readonly CrmContext _db;
    private readonly IMapper _mapper;
    private readonly ICompanyService _companyService;


    public BenefitService(CrmContext db, IMapper mapper, ICompanyService companyService)
    {
        _db = db;
        _mapper = mapper;
        _companyService = companyService;
    }

    public async Task<IEnumerable<Benefit>> GetAll()
    {
        return await _db.Benefits
            .Where(c => c.IsDeleted == false)
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .ToListAsync();
    }
    
    public async Task<Benefit?> GetById(int benefitId)
    {
        return await _db.Benefits
            .Include(c => c.Companies)
            .Include(c => c.Applications)
            .FirstOrDefaultAsync(c => c.Id == benefitId && c.IsDeleted == false);
    }
    
    public async Task<IEnumerable<Benefit>> GetByCompanyId (int companyId)
    {
        var company = await _companyService.GetById(companyId);
        if (company != null)
            return company.Benefits.Where(c => !c.IsDeleted);
        return null;
    }

    public async Task Create(BenefitCreateViewModel model)
    {
        var benefit = _mapper.Map<BenefitCreateViewModel, Benefit>(model);
        _db.Benefits.Add(benefit);
        await _db.SaveChangesAsync();
    }

    public async Task<bool> Delete(int benefitId)
    {
        var benefit = await GetById(benefitId);
        
        if (benefit != null)
        {
            benefit.IsDeleted = true;

            _db.Benefits.Update(benefit);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> Update(BenefitViewModel model)
    {
        var benefit = await GetById(model.Id);

        if (benefit != null)
        {
            benefit.Name = model.Name;
            
            _db.Benefits.Update(benefit);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

    public async Task<bool> IsExists(string benefitName)
    {
        return await _db.Benefits
            .AnyAsync(r => r.Name == benefitName && r.IsDeleted == false);
    }
}