﻿using HRCRM.Models;
using HRCRM.Models.ViewModels;

namespace HRCRM.Services.BenefitServices;

public interface IBenefitService
{
    public Task<IEnumerable<Benefit>> GetAll();
    public Task<Benefit?> GetById(int benefitId);
    public Task Create (BenefitCreateViewModel model);
    public Task<bool> Delete (int benefitId);
    public Task<bool> Update (BenefitViewModel model);
    public Task<bool> IsExists(string benefitName);
    public Task<IEnumerable<Benefit>> GetByCompanyId (int companyId);
}