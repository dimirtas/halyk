﻿using HRCRM.Models;

namespace HRCRM.Services.CommentServices;

public interface ICommentService
{
    public Task<IEnumerable<Comment>> GetAll();
    public Task<Comment?> GetById(int id);
    public Task Create (Comment model);
    public Task Delete (Comment model);
    public Task<bool> Update (Comment model);
}