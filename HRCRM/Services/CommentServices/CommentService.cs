﻿using HRCRM.Models;
using HRCRM.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Services.CommentServices;

public class CommentService : ICommentService
{
    private readonly CrmContext _db;

    public CommentService(CrmContext db)
    {
        _db = db;
    }
    
    public async Task<IEnumerable<Comment>> GetAll()
    {
        return await _db.Comments
            .Include(c => c.Resume)
            .Include(c => c.Author)
            .Where(a => !a.IsDeleted)
            .ToListAsync();
    }
    
    public async Task<Comment?> GetById(int id)
    {
        return await _db.Comments
            .Include(c => c.Resume)
            .Include(c => c.Author)
            .Where(c => !c.IsDeleted && c.Id == id).FirstOrDefaultAsync();
    }
    
    public async Task Create(Comment model)
    {
        await _db.Comments.AddAsync(model);
        await _db.SaveChangesAsync();
    }

    public async Task Delete(Comment model)
    {
        model.IsDeleted = true;
        await Update(model);
    }

    public async Task<bool> Update(Comment model)
    {
        var comment = await GetById(model.Id);

        if (comment != null)
        {
            comment.editDateTime = DateTime.UtcNow;
            comment.MessageText = model.MessageText;
            _db.Comments.Update(comment);
            await _db.SaveChangesAsync();
            return true;
        }
        
        return false;
    }

}