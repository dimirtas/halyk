﻿namespace HRCRM.Models;

public class Company
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Bin { get; set; }
    public bool IsDeleted { get; set; } = false;
    public List<User> Users { get; set; } = new();
    public List<City> Cities { get; set; } = new();
    public List<Department> Departments { get; set; } = new();
    public List<Division> Divisions { get; set; } = new();
    public List<Benefit> Benefits { get; set; } = new();
    public List<Knowledge> Knowledges{ get; set; } = new();
    public List<Skill> Skills{ get; set; } = new();
    public List<Ability> Abilities{ get; set; }  = new();
    public List<Quality> Qualities { get; set; } = new();
    
    public List<Application> Applications { get; set; } = new();
    
    public List<Vacancy> Vacancies { get; set; } = new();
}