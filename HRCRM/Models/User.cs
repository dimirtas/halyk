﻿namespace HRCRM.Models;

public class User
{
    public int Id { get; set; }
    public string Username { get; set; } 
    public string Password { get; set; } 
    public string Email { get; set; } 
    public string FirstName { get; set; } 
    public string SecondName { get; set; } 
    public string Position { get; set; } 
    public List<Role> Roles { get; set; } = new();
    public string Phone { get; set; }
    public string? RefreshToken { get; set; }
    public DateTime RefreshTokenExpiryTime { get; set; } 
    public int? CompanyId { get; set; }
    public Company Company { get; set; }
    public bool IsDeleted { get; set; }
}