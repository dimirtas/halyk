﻿namespace HRCRM.Models;

public class Resume
{
    public int Id { get; set; }
    public string SenderEmail { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public bool IsDeleted { get; set; } = false;
    
    public DateTime CreateResumeDate { get; set; } = DateTime.UtcNow;
    public string FileResume { get; set; }
    public string MessageUid { get; set; }
    public string FileName { get; set; }
    public int VacancyId { get; set; }
    public Vacancy Vacancy { get; set; }
    public int CompanyId { get; set; }
    public Company Company { get; set; }

    public List<Comment> Comments { get; set; } = new();
}

