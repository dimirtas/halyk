using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum LaborContract
{
    [Display(Name = "Постоянный")]
    Permanent = 1,
    
    [Display(Name = "Временный")]
    Temporary = 2
}