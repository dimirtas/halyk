using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum OpenReason
{
    [Display(Name = "Замена сотрудника")]
    Replacement = 1,
    
    [Display(Name = "Замена декретницы")]
    MaternityLeave = 2,
    
    [Display(Name = "Новыя позиция")]
    NewPosition = 3
}