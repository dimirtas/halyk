using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum WorkExperience
{
    [Display(Name = "Без опыта")]
    WithoutExperience = 1,
    
    [Display(Name = "От 1 года")]
    FromOne = 2,
    
    [Display(Name = "От 1 до 2 года")]
    FromOneToTwo = 3,
    
    [Display(Name = "От 2 до 3 лет")]
    FromTwoToThree = 4,
    
    [Display(Name = "От 3 до 5 лет")]
    FromThreeToFive = 5,
    
    [Display(Name = "От 5 лет")]
    FromFive = 6
}