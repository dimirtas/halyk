﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum WorkingСonditions
{
    [Display(Name = "5 -дневная рабочая неделя")]
    FiveDayWorkingWeek = 1,
    
    [Display(Name = "6-дневная рабочая неделя")]
    SixDayWorkingWeek = 2,
    
    [Display(Name = "Сменная работа")]
    ShiftWork = 3,
    
    [Display(Name = "Свободный график")]
    FreeSchedule = 4,
    
    [Display(Name = "Вахтовый метод работы")]
    ShiftMethodWork = 5

}
