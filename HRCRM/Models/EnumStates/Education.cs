using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum Education
{
    [Display(Name = "Релевантное высшее")]
    RelevantHigher = 1,
    
    [Display(Name = "Высшее")]
    Higher = 2,
    
    [Display(Name = "Среднее-специальное")]
    SpecializedSecondary = 3,
    
    [Display(Name = "Не имеет значения")]
    Irrelevant = 4
}