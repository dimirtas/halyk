using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum EnglishProficiency
{ 
    [Display(Name = "Элементарный")]
    Elementary = 1,
    
    [Display(Name = "Средний")]
    Intermediate = 2,
    
    [Display(Name = "Выше среднего")]
    UppperIntermediate = 3,
    
    [Display(Name = "Продвинутый")]
    Advanced = 4,
    
    [Display(Name = "В совершенстве")]
    Proficiency = 5
}