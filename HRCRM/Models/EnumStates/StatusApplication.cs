using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.EnumStates;

public enum StatusApplication
{
    [Display(Name = "Новая")]
    Waiting = 1,
    
    [Display(Name = "Отклонена")]
    Rejected = 2,
    
    [Display(Name = "Одобрена")]
    Approved = 3
}
