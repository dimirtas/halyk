﻿namespace HRCRM.Models;

public class Knowledge
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; } = false;
    public List<Company> Companies { get; set; } 
    public List<Vacancy> Vacancies { get; set; } = new();
}