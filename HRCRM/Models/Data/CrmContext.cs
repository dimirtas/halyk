﻿using System.Net.Mime;
using Microsoft.EntityFrameworkCore;

namespace HRCRM.Models.Data;

public class CrmContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Company> Companies { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<City>  Cities { get; set; }
    public DbSet<Benefit> Benefits { get; set; }
    public DbSet<Department> Departments{ get; set; }
    public DbSet<Division> Divisions{ get; set; }
    public DbSet<Application> Applications { get; set; }
    public DbSet<Knowledge> Knowledges { get; set; }
    public DbSet<Skill> Skills { get; set; }
    public DbSet<Ability> Abilities { get; set; }
    public DbSet<Quality> Qualities { get; set; }
    public DbSet<Vacancy> Vacancies { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<Resume> Resumes { get; set; }
   

    public CrmContext(DbContextOptions<CrmContext> options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Role>().HasData(
            new Role() { Id = 1, Name = "admin" },
            new Role() { Id = 2, Name = "company" },
            new Role() { Id = 3, Name = "hrmanager" },
            new Role() { Id = 4, Name = "hiring" }
        );
        modelBuilder.Entity<City>().HasData(
            new City() { Id = 1, Name = "Алматы"},
            new City() { Id = 2, Name = "Нур-Султан"},
            new City() { Id = 3, Name = "Шымкент"},
            new City() { Id = 4, Name = "Тараз"},
            new City() { Id = 5, Name = "Талдыкорган"},
            new City() { Id = 6, Name = "Кызылорда"},
            new City() { Id = 7, Name = "Туркестан"},
            new City() { Id = 8, Name = "Жезказган"},
            new City() { Id = 9, Name = "Семей"},
            new City() { Id = 10, Name = "Актау"},
            new City() { Id = 11, Name = "Атырау"},
            new City() { Id = 12, Name = "Актобе"},
            new City() { Id = 13, Name = "Костанай"},
            new City() { Id = 14, Name = "Петропавловск"},
            new City() { Id = 15, Name = "Кокшетау"},
            new City() { Id = 16, Name = "Экибазтуз"},
            new City() { Id = 17, Name = "Павлодар"},
            new City() { Id = 18, Name = "Усть-Каменогорск"}
        );
        modelBuilder.Entity<Benefit>().HasData(
            new Benefit() { Id = 1, Name = "Автомобиль"},
            new Benefit() { Id = 2, Name = "Мобильный телефон"},
            new Benefit() { Id = 3, Name = "Питание"},
            new Benefit() { Id = 4, Name = "Лимит на сотовую связь"},
            new Benefit() { Id = 5, Name = "Медицинская страховка"},
            new Benefit() { Id = 6, Name = "Больничный без документов"},
            new Benefit() { Id = 7, Name = "Компенсация за отказ от курения"},
            new Benefit() { Id = 8, Name = "Оплата парковки"},
            new Benefit() { Id = 9, Name = "Оплата интернета"},
            new Benefit() { Id = 10, Name = "Декретные выплаты"},
            new Benefit() { Id = 11, Name = "Дополнительный отпуск"},
            new Benefit() { Id = 12, Name = "Home office"},
            new Benefit() { Id = 13, Name = "Оплата больничного"},
            new Benefit() { Id = 14, Name = "Оплата фитнеса"}
        );
        modelBuilder.Entity<Knowledge>().HasData(
            new Knowledge() { Id = 1, Name = "Амбициозность"},
            new Knowledge() { Id = 2, Name = "Аналитические способности"},
            new Knowledge() { Id = 3, Name = "Быстрая обучаемость"},
            new Knowledge() { Id = 4, Name = "Гибкость"},
            new Knowledge() { Id = 5, Name = "Инициативность"},
            new Knowledge() { Id = 6, Name = "Коммуникабельность"},
            new Knowledge() { Id = 7, Name = "Креативность"},
            new Knowledge() { Id = 8, Name = "Нацеленность на результат"},
            new Knowledge() { Id = 9, Name = "Организаторские способности"},
            new Knowledge() { Id = 10, Name = "Работа в условиях многозадачности"},
            new Knowledge() { Id = 11, Name = "Развитое стратегическое мышление"},
            new Knowledge() { Id = 12, Name = "Самоконтроль"},
            new Knowledge() { Id = 13, Name = "Самообучаемость"},
            new Knowledge() { Id = 14, Name = "Стрессоустойчивость"},  
            new Knowledge() { Id = 15, Name = "Уверенность"},
            new Knowledge() { Id = 16, Name = "Умение адаптироваться к переменам"},
            new Knowledge() { Id = 17, Name = "Умение находить общий язык с людьми"},
            new Knowledge() { Id = 18, Name = "Умение работать в команде"},
            new Knowledge() { Id = 19, Name = "Умение работать с большим объемом информации"}
        );
        
        modelBuilder.Entity<Ability>().HasData(
            new Ability() { Id = 1, Name = "Большой опыт"},
            new Ability() { Id = 2, Name = "Специалист широкого профиля"},
            new Ability() { Id = 3, Name = "Владение дополнительными навыками"},
            new Ability() { Id = 4, Name = "Способность проведения ремонтных работ"},
            new Ability() { Id = 5, Name = "Опыт работы с персоналом"},
            new Ability() { Id = 6, Name = "Подготовка проектов"}
        );
        
        modelBuilder.Entity<Skill>().HasData(
            new Skill() { Id = 1, Name = "категории вождения D/F/E"},
            new Skill() { Id = 2, Name = "Знание правил таможенной очистки"},
            new Skill() { Id = 3, Name = "Готов к длительным командировкам"},
            new Skill() { Id = 4, Name = "Есть опыт в маркетинге"},
            new Skill() { Id = 5, Name = "Обладает лидерскими качествами"},
            new Skill() { Id = 6, Name = "Владение другими иностранными языками"}
        );
        
        modelBuilder.Entity<Quality>().HasData(
            new Quality() { Id = 1, Name = "Активный"},
            new Quality() { Id = 2, Name = "Целеустремленный"},
            new Quality() { Id = 3, Name = "Не уверенный"},
            new Quality() { Id = 4, Name = "Перспективный"},
            new Quality() { Id = 5, Name = "Работоспособный"},
            new Quality() { Id = 6, Name = "Мотивированный"}
        );
        
        modelBuilder.Entity<Department>().HasData(
            new Department() { Id = 1, Name = "Транспортный"},
            new Department() { Id = 2, Name = "Логистический"},
            new Department() { Id = 3, Name = "Ремонтный"},
            new Department() { Id = 4, Name = "Продаж"},
            new Department() { Id = 5, Name = "Маркетинга"},
            new Department() { Id = 6, Name = "Бухгалтерия"}
        );
        
        modelBuilder.Entity<Division>().HasData(
            new Division() { Id = 1, Name = "Финансовый"},
            new Division() { Id = 2, Name = "Коммерческий"},
            new Division() { Id = 3, Name = "Логистики"},
            new Division() { Id = 4, Name = "Маркетинга и продвижения"}
        );
        
        modelBuilder.Entity<Company>().HasData(
            new Company() { Id = 1, Name = "ТОО Avis Logistic", Bin = "12345678"},
            new Company() { Id = 2, Name = "АО Агро-продукт", Bin = "87654321"}
        );
            
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Abilities)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { AbilitiesId = 1, CompaniesId = 1 },
                new { AbilitiesId = 2, CompaniesId = 1 },
                new { AbilitiesId = 3, CompaniesId = 1 },
                new { AbilitiesId = 4, CompaniesId = 2 },
                new { AbilitiesId = 5, CompaniesId = 2 },
                new { AbilitiesId = 6, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Cities)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { CitiesId = 1, CompaniesId = 1 },
                new { CitiesId = 2, CompaniesId = 1 },
                new { CitiesId = 3, CompaniesId = 1 },
                new { CitiesId = 4, CompaniesId = 1 },
                new { CitiesId = 5, CompaniesId = 1 },
                new { CitiesId = 4, CompaniesId = 2 },
                new { CitiesId = 5, CompaniesId = 2 },
                new { CitiesId = 6, CompaniesId = 2 },
                new { CitiesId = 7, CompaniesId = 2 },
                new { CitiesId = 8, CompaniesId = 2 },
                new { CitiesId = 9, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Departments)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { DepartmentsId = 1, CompaniesId = 1 },
                new { DepartmentsId = 2, CompaniesId = 1 },
                new { DepartmentsId = 3, CompaniesId = 1 },
                new { DepartmentsId = 4, CompaniesId = 2 },
                new { DepartmentsId = 5, CompaniesId = 2 },
                new { DepartmentsId = 6, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Divisions)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { DivisionsId = 1, CompaniesId = 1 },
                new { DivisionsId = 4, CompaniesId = 1 },
                new { DivisionsId = 3, CompaniesId = 2 },
                new { DivisionsId = 2, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Benefits)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { BenefitsId = 1, CompaniesId = 1 },
                new { BenefitsId = 2, CompaniesId = 1 },
                new { BenefitsId = 3, CompaniesId = 1 },
                new { BenefitsId = 4, CompaniesId = 1 },
                new { BenefitsId = 5, CompaniesId = 1 },
                new { BenefitsId = 6, CompaniesId = 1 },
                new { BenefitsId = 1, CompaniesId = 2 },
                new { BenefitsId = 2, CompaniesId = 2 },
                new { BenefitsId = 3, CompaniesId = 2 },
                new { BenefitsId = 4, CompaniesId = 2 },
                new { BenefitsId = 5, CompaniesId = 2 },
                new { BenefitsId = 6, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Knowledges)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { KnowledgesId = 1, CompaniesId = 1 },
                new { KnowledgesId = 2, CompaniesId = 1 },
                new { KnowledgesId = 3, CompaniesId = 1 },
                new { KnowledgesId = 4, CompaniesId = 1 },
                new { KnowledgesId = 5, CompaniesId = 1 },
                new { KnowledgesId = 6, CompaniesId = 1 },
                new { KnowledgesId = 5, CompaniesId = 2 },
                new { KnowledgesId = 7, CompaniesId = 2 },
                new { KnowledgesId = 8, CompaniesId = 2 },
                new { KnowledgesId = 9, CompaniesId = 2 },
                new { KnowledgesId = 10, CompaniesId = 2 },
                new { KnowledgesId = 11, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Qualities)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { QualitiesId = 1, CompaniesId = 1 },
                new { QualitiesId = 2, CompaniesId = 1 },
                new { QualitiesId = 3, CompaniesId = 1 },
                new { QualitiesId = 4, CompaniesId = 2 },
                new { QualitiesId = 5, CompaniesId = 2 },
                new { QualitiesId = 6, CompaniesId = 2 }
            ));
        
        modelBuilder.Entity<Company>()
            .HasMany(c => c.Skills)
            .WithMany(x => x.Companies)
            .UsingEntity(j => j.HasData (
                new { SkillsId = 1, CompaniesId = 1 },
                new { SkillsId = 2, CompaniesId = 1 },
                new { SkillsId = 3, CompaniesId = 1 },
                new { SkillsId = 4, CompaniesId = 2 },
                new { SkillsId = 5, CompaniesId = 2 },
                new { SkillsId = 6, CompaniesId = 2 }
            ));

        
        //todo ApplyConfiguration
        base.OnModelCreating(modelBuilder);
    }
}