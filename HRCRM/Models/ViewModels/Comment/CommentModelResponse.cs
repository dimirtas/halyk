﻿using HRCRM.Models.ViewModels.Resume;
using HRCRM.Models.ViewModels.User;

namespace HRCRM.Models.ViewModels.Commit;

public class CommentModelResponse
{
    public int Id { get; set; }
    public bool IsDeleted { get; set; } = false;
    public string MessageText { get; set; }
    public DateTime editDateTime { get; set; }

    public int AuthorId{get; set;}
    public UserModelResponse User { get; set; }
    
    public int ResumeId { get; set; }
    public ResumeModelResponse Resume { get; set; }
    
}