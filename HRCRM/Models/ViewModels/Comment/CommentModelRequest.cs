﻿namespace HRCRM.Models.ViewModels.Commit;

public class CommentModelRequest
{
    public int Id { get; set; }
    public string MessageText { get; set; }
    public DateTime editDateTime { get; set; }
    public int AuthorId {get; set; }
    public int ResumeId { get; set; }
    
}