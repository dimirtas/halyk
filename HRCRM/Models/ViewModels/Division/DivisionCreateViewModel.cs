﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class DivisionCreateViewModel
{
    
    [Required(ErrorMessage = "Введите название роли")]
    public string Name { get; set; }
}