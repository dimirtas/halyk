﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class RoleViewModel
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название роли")]
    public string Name { get; set; }
}