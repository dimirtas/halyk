﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class RoleNewViewModel
{
    
    [Required(ErrorMessage = "Введите название роли")]
    public string Name { get; set; }
}