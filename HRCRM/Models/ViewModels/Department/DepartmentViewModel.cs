﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class DepartmentViewModel
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название льготы")]
    public string Name { get; set; }
}