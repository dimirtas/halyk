﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class DepartmentCreateViewModel
{
    [Required(ErrorMessage = "Введите название льготы")]
    public string Name { get; set; }
    
}