﻿using OpenPop.Mime;

namespace HRCRM.Models.ViewModels.Resume;

public class MessageUidModel
{
    public int Number { get; set; }
    public string Uid { get; set; }
    public Message Message { get; set; }
}