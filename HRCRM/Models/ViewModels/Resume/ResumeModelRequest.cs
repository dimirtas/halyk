﻿namespace HRCRM.Models.ViewModels.Resume;

public class ResumeModelRequest
{
    public int Id { get; set; }
    public string SenderEmail { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public DateTime CreateResumeDate { get; set; }
    public string FileName { get; set; }
    public string FileResume { get; set; }
    public int VacancyId { get; set; }
    public int CompanyId { get; set; }
}