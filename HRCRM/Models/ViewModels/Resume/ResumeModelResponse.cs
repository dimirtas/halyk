﻿using HRCRM.Models.ViewModels.Vacancy;

namespace HRCRM.Models.ViewModels.Resume;

public class ResumeModelResponse
{
    public int Id { get; set; }
    public bool IsDeleted { get; set; } = false;
    public string Name { get; set; }
    public string Surname { get; set; }
    public DateTime CreateResumeDate { get; set; }
    public string SenderEmail { get; set; }
    public string FileName { get; set; }
    public string FileResume { get; set; }

    public int VacancyId { get; set; }
    public VacancyModelResponse Vacancy { get; set; }
    
    public int CompanyId { get; set; }
    public CompanyViewModel Company { get; set; }
}