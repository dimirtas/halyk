﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class KnowledgeModelResponse
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название знания")]
    public string Name { get; set; }
}