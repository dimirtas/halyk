using HRCRM.Models.ViewModels.User;

namespace HRCRM.Models.ViewModels.Auth;

public class LoginModelResponse
{
    public UserModelResponse User { get; set; }
    public string RefreshToken { get; set; }
    public string AccessToken { get; set; }
    public DateTime LifeTimeAccessToken { get; set; }
}