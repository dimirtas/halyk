﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class RegisterViewModel
{
    /*[Required(ErrorMessage = "Введите эл. адрес")]
    [DataType(DataType.EmailAddress)]
    [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
    [Display(Name = "Электронная почта")]*/
    public string Email { get; set; }
    /*[DataType(DataType.Password)]
    [Required(ErrorMessage = "Вы не ввели пароль")]
    [Display(Name = "Пароль")]*/
    public string? Password { get; set; }
    /*[DataType(DataType.Password)]
    [Display(Name = "Подтверждение пароля")]
    [Compare("Password", ErrorMessage = "Пароли не совпадают")]*/
    // public string ConfirmPassword { get; set; }
    /*[Required(ErrorMessage = "Поле имя не может быть пустым")]
    [StringLength(40, MinimumLength = 2, ErrorMessage = "Имя не может быть короче 2 символов и длиннее 40")]
    [DataType(DataType.Text)]
    [Display(Name = "Имя")]*/
    public string FirstName { get; set; }
    /*[Required(ErrorMessage = "Поле фамилия не может быть пустым")]
    [StringLength(40, MinimumLength = 1, ErrorMessage = "Фамилия не может быть короче 1 символа и длиннее 40")]
    [DataType(DataType.Text)]
    [Display(Name = "Фамилия")]*/
    public string SecondName { get; set; }
    /*[Required(ErrorMessage = "Поле должность не может быть пустым")]
    [StringLength(40, MinimumLength = 2, ErrorMessage = "Должность не может быть короче 2 символа и длиннее 40")]
    [DataType(DataType.Text)]
    [Display(Name = "Должность")]*/
    public string Position { get; set; }

    /*[Required(ErrorMessage = "Необходимо указать номер телефона")]
    [Range(77000000000, 87999999999, ErrorMessage = "Укажите номер в формате 77#########")]
    [DataType(DataType.PhoneNumber)]
    [Display(Name = "Номер телефона")]*/
    public string Phone { get; set; }

    /*[Required(ErrorMessage = "Необходимо выбрать хотя бы одну роль")]
    [Display(Name = "Роли")]*/
    public List<int> RoleIds { get; set; } = new();

    /*[DataType(DataType.Text)]
    [Display(Name = "Компания")]*/
    public int? CompanyId { get; set; }
}