﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class CityCreateViewModel
{
    [Required(ErrorMessage = "Введите название города")]
    public string Name { get; set; }
    
}