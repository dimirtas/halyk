﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class CityViewModel
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название города")]
    public string Name { get; set; }
}