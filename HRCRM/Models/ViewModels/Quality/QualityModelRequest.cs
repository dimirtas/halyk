﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class QualityModelRequest
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название качества ")]
    public string Name { get; set; }
}