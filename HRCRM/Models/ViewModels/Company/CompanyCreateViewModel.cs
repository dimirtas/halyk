﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class CompanyCreateViewModel
{
    [Required(ErrorMessage = "Введите название организации")]
    public string Name { get; set; }
    [Required(ErrorMessage = "Введите БИН/ИНН организации")]
    public string Bin { get; set; }
}