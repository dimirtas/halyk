using HRCRM.Models.EnumStates;

namespace HRCRM.Models.ViewModels.Application;

public class ApplicationModelRequest
{
    public int Id { get; set; }
    public string Position { get; set; }
    public DateTime EmploymentDate { get; set; }
    public int CityId { get; set; }
    public int DivisionId { get; set; }
    public int DepartmentId { get; set; }
    public int LineManagerId  { get; set; }
    public OpenReason OpenReason { get; set; }
    public LaborContract LaborContract { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    public Education Education { get; set; }
    public StatusApplication StatusApplication { get; set; }
    public string OtherKnowledge { get; set; }
    public decimal Salary { get; set; }
    public int[] BenefitIds { get; set; }
    public int CompanyId { get; set; }
}