using HRCRM.Models.EnumStates;
using HRCRM.Models.ViewModels.User;

namespace HRCRM.Models.ViewModels.Application;

public class ApplicationModelResponse
{
    public int Id { get; set; }
    public string Position { get; set; }
    public DateTime EmploymentDate { get; set; }
    public int CityId { get; set; }
    public CityViewModel City { get; set; }
    public int DivisionId { get; set; }
    public DivisionViewModel Division { get; set; }
    public int DepartmentId { get; set; }
    public DepartmentViewModel Department { get; set; }
    public int LineManagerId  { get; set; }
    public UserModelResponse LineManager { get; set; }
    public int CompanyId { get; set; }
    public CompanyViewModel Company { get; set; }
    public OpenReason OpenReason { get; set; }
    public LaborContract LaborContract { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    public Education Education { get; set; }
    public StatusApplication StatusApplication { get; set; }
    public string OtherKnowledge { get; set; }
    public decimal Salary { get; set; }
    public List<BenefitViewModel> BenefitIds { get; set; }
}