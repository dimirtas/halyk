﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels;

public class SkillModelRequest
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Введите название Способности")]
    public string Name { get; set; }
}