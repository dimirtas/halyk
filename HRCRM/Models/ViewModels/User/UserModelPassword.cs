﻿namespace HRCRM.Models.ViewModels.User;

public class UserModelPassword
{
    public string Email { get; set; }
    public string checkingCurrentPassword { get; set; }
    public string newPassword { get; set; }
}