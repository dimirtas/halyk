﻿using System.ComponentModel.DataAnnotations;

namespace HRCRM.Models.ViewModels.User;

public class UserModelRequest
{
    public int Id { get; set; }
    [Required(ErrorMessage = "Поле почты не может быть пустым")]
    public string Email { get; set; } 
    [Required(ErrorMessage = "Поле имя не может быть пустым")]
    public string FirstName { get; set; } 
    [Required(ErrorMessage = "Поле фамилия не может быть пустым")]
    public string SecondName { get; set; } 
    [Required(ErrorMessage = "Поле должность не может быть пустым")]
    public string Position { get; set; } 
    [Required(ErrorMessage = "Поле телефон не может быть пустым")]
    public string Phone { get; set; } 
    public int[] RoleIds { get; set; }
    [Required(ErrorMessage = "Поле компании не может быть пустым")]
    public int CompanyId { get; set; }
}