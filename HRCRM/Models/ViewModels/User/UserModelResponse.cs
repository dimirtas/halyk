﻿namespace HRCRM.Models.ViewModels.User;

public class UserModelResponse
{
    public int Id { get; set; }
    public string Username { get; set; } 
    public string Email { get; set; } 
    public string FirstName { get; set; } 
    public string SecondName { get; set; } 
    public string Position { get; set; }
    public string Phone { get; set; }
    public int? CompanyId { get; set; }
    public CompanyViewModel Company { get; set; }
    public List<RoleViewModel> RoleIds { get; set; }
}

