using HRCRM.Models.EnumStates;
using HRCRM.Models.ViewModels.User;

namespace HRCRM.Models.ViewModels.Vacancy;

public class VacancyModelResponse
{
    public int Id { get; set; }
    public bool IsDeleted { get; set; } = false;
    public string Position { get; set; }
    public int CountOfEmployees { get; set; }
    public string PositionMission { get; set; }
    public string Responsibilities { get; set; }
    public string ExpectedResults { get; set; }
    public string Kpi { get; set; }
    public string CareerOpportunities { get; set; }
    public string CommentCareerOpportunities { get; set; }
    public string Requirements { get; set; }
    public decimal Salary { get; set; }

    public int CityId { get; set; }
    public CityViewModel City { get; set; }
    
    public int DivisionId { get; set; }
    public DivisionViewModel Division { get; set; }
    
    public int DepartmentId { get; set; }
    public DepartmentViewModel Department { get; set; }
    
    public int PositionDirectManagerId  { get; set; }
    public UserModelResponse PositionDirectManager { get; set; }
    
    public int ManagerOfTheManagerId  { get; set; }
    public UserModelResponse ManagerOfTheManager { get; set; }
    
    public int CompanyId { get; set; }
    public CompanyViewModel Company { get; set; }

    public DateTime EmploymentDate { get; set; } = DateTime.UtcNow;
    public LaborContract LaborContract { get; set; }
    public OpenReason OpenReason { get; set; }
    public WorkingСonditions WorkingСonditions { get; set; }
    public WorkExperience WorkExperience { get; set; }
    public Education Education { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    
    public List<KnowledgeModelResponse> KnowledgeIds { get; set; }
    public List<SkillModelResponse> SkillIds { get; set; }
    public List<AbilityModelResponse> AbilityIds { get; set; }
    public List<QualityModelResponse> QualityIds { get; set; }
    public List<BenefitViewModel> BenefitIds { get; set; }

}
