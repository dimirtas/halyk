using HRCRM.Models.EnumStates;

namespace HRCRM.Models.ViewModels.Vacancy;

public class VacancyModelRequest
{
    
    public int Id { get; set; }
    public string Position { get; set; }
    public int CityId { get; set; }
    public int DivisionId { get; set; }
    public int DepartmentId { get; set; }
    public int PositionDirectManagerId { get; set; }
    public int ManagerOfTheManagerId { get; set; }
    public int CountOfEmployees { get; set; }
    public string PositionMission { get; set; }
    public string Responsibilities { get; set; }
    public string ExpectedResults { get; set; }
    public string Kpi { get; set; }
    public string CareerOpportunities { get; set; }
    public string CommentCareerOpportunities { get; set; }
    public string Requirements { get; set; }
    public decimal Salary { get; set; }
    public DateTime EmploymentDate { get; set; }
    public LaborContract LaborContract { get; set; }
    public OpenReason OpenReason { get; set; }
    public WorkingСonditions WorkingСonditions { get; set; }
    public WorkExperience WorkExperience { get; set; }
    public Education Education { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    public int[] KnowledgeIds { get; set; }
    public int[] SkillIds { get; set; }
    public int[] AbilityIds { get; set; }
    public int[] QualityIds { get; set; }
    public int[] BenefitIds { get; set; }
    
    public int CompanyId { get; set; }

}