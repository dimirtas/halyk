﻿using HRCRM.Models.EnumStates;

namespace HRCRM.Models;

public class Application
{
    public int Id { get; set; }
    public bool IsDeleted { get; set; } = false;
    public string Position { get; set; }
    public DateTime EmploymentDate { get; set; } = DateTime.UtcNow;
    public int CityId { get; set; }
    public City City { get; set; }
    public int DivisionId { get; set; }
    public Division Division { get; set; }
    public int DepartmentId { get; set; }
    public Department Department { get; set; }
    public int LineManagerId  { get; set; }
    public User LineManager { get; set; }
    public OpenReason OpenReason { get; set; }
    public LaborContract LaborContract { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    public StatusApplication StatusApplication { get; set; }
    public Education Education { get; set; }
    public string OtherKnowledge { get; set; }
    public decimal Salary { get; set; }
    public List<Benefit> Benefits { get; set; }
    public int CompanyId { get; set; }
    public Company Company { get; set; }
}