﻿namespace HRCRM.Models;

public class Comment
{
    public int Id { get; set; }
    public string MessageText { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTime editDateTime { get; set; } = DateTime.Now;
    
    public int AuthorId {get; set;}
    public User Author { get; set; }

    public int ResumeId { get; set; }
    public Resume Resume { get; set; }
}

