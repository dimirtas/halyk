﻿using HRCRM.Models.EnumStates;

namespace HRCRM.Models;

public class Vacancy
{
    public int Id { get; set; }
    public bool IsDeleted { get; set; } = false;
    public string Position { get; set; }
    public int CityId { get; set; }
    public City City { get; set; }
    public int DivisionId { get; set; }
    public Division Division { get; set; }
    public int DepartmentId { get; set; }
    public Department Department { get; set; }
    public int? PositionDirectManagerId  { get; set; }
    public User PositionDirectManager { get; set; }
    public int? ManagerOfTheManagerId  { get; set; }
    public User ManagerOfTheManager { get; set; }
    public int? CountOfEmployees { get; set; }
    public string? PositionMission { get; set; }
    public string? Responsibilities { get; set; }
    public string? ExpectedResults { get; set; }
    public string? Kpi { get; set; }
    public string? CareerOpportunities { get; set; }
    public string? CommentCareerOpportunities { get; set; }
    public string? Requirements { get; set; }
    public decimal? Salary { get; set; }
    public LaborContract LaborContract { get; set; }
    public OpenReason OpenReason { get; set; }
    public WorkingСonditions WorkingСonditions { get; set; }
    public WorkExperience WorkExperience { get; set; }
    public Education Education { get; set; }
    public EnglishProficiency EnglishProficiency { get; set; }
    public List<Knowledge> Knowledges{ get; set; }
    public List<Skill> Skills{ get; set; }
    public List<Ability> Abilities { get; set; }
    public List<Quality> Qualities { get; set; }
    public List<Benefit> Benefits { get; set; }
    public int CompanyId { get; set; }
    public Company Company { get; set; }

}