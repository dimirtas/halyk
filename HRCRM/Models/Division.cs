﻿namespace HRCRM.Models;

public class Division
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; } = false;
    public List<Company> Companies { get; set; } = new();
    public List<Application> Applications { get; set; } = new();
}