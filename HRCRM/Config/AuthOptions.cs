using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace HRCRM.Config;

public class AuthOptions
{
    public const string Issuer = "HRProgect"; // издатель токена
    public const string Audience = "AuthClient"; // потребитель токена
    const string Key = "mysupersecret_secretkey!123";   // ключ для шифрации
    public const int LifeTimeTokenInMinute = 5;
    public const int LifeTimeRefreshTokenInMinute = 15;
    public static SymmetricSecurityKey GetSymmetricSecurityKey() => 
        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
}