using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.RoleServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class RolesController : ControllerBase
{
    private readonly IRoleService _roleService;
    private readonly IMapper _mapper;

    public RolesController(IRoleService roleService, IMapper mapper)
    {
        _roleService = roleService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<RoleViewModel>>> Get()
    {
        var roles = await _roleService.GetAll();
        var result =  _mapper.Map<IEnumerable<RoleViewModel>>(roles);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<RoleViewModel>> Get(int id)
    {
        var role = await _roleService.GetById(id);
        if (role is null)
            return BadRequest("Роль с таким id не найдена");
        var result = _mapper.Map<RoleViewModel>(role);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(RoleNewViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _roleService.IsExists(model.Name))
            return BadRequest("Данная роль уже существует");

        await _roleService.Create(model);
        return NoContent();
    }
    
    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _roleService.Delete(id);
        if (!check)
            return BadRequest("Нет такой роли");
        
        return NoContent();
    }

    [HttpPut("update")]
    public async Task<ActionResult> Update(RoleViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _roleService.IsExists(model.Name))
            return BadRequest("Данная роль уже существует");

        bool check =  await _roleService.Update(model);
        if (!check)
            return BadRequest("Нет такой роли");
        
        return NoContent();
    }
}