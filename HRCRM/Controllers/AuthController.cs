using HRCRM.Models.ViewModels;
using HRCRM.Services.AuthServices;
using HRCRM.Services.TokenServices;
using HRCRM.Services.UserServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class AuthController : Controller
{
    private readonly IAuthService _authService;
    private readonly ITokenService _tokenService;
    private readonly IUserService _userService;

    public AuthController(IAuthService authService, ITokenService tokenService, IUserService userService)
    {
        _authService = authService;
        _tokenService = tokenService;
        _userService = userService;
    }

    [HttpPost("login")]
    public async Task<ActionResult> Login(LoginViewModel model)
    {
        var response = await _authService.Login(model);
        if (response == null)
            return Unauthorized("Некорректный логин или пароль");
        return Ok(response);
    }

    [HttpPost("refreshToken")]
    public async Task<ActionResult> RefreshToken(string token)
    {
        var newRefreshToken = await _tokenService.RefreshToken(token);
        if (newRefreshToken == null)
            return BadRequest("Что-то пошло не так");
        return Ok(newRefreshToken);
    }

    [HttpPost("register")]
    public async Task<ActionResult> Register(RegisterViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _userService.IsEmailExists(model.Email))
            return BadRequest("Данный Email уже существует");
        
        if (await _userService.IsPhoneNumberExists(model.Phone))
            return BadRequest("Данный номер телефона уже существует");

        await _authService.Register(model);

        return NoContent();
    }
    
    [HttpPost("logout")]
    public async Task<ActionResult> Logout(int id)
    {
        bool response = await _authService.Logout(id);
        if (!response)
            return BadRequest("Произошла ошибка");
        return NoContent();
    }
}