using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.QualityServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class QualitiesController : ControllerBase
{
    private readonly IQualityService _qualityService;

    public QualitiesController(IQualityService qualityService, IMapper mapper)
    {
        _qualityService = qualityService;
        _mapper = mapper;
    }

    private readonly IMapper _mapper;

    [HttpGet]
    public async Task<ActionResult<IEnumerable<QualityModelResponse>>> Get()
    {
        var qualities = await _qualityService.GetAll();
        var result = _mapper.Map<IEnumerable<QualityModelResponse>>(qualities);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<QualityModelResponse>> Get(int id)
    {
        var quality = await _qualityService.GetById(id);
        if (quality == null)
            return BadRequest("Качество с таким id не найдено");
        var result = _mapper.Map<QualityModelResponse>(quality);
        return Ok(result);
    }
    
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Quality>>> GetByCompanyId(int companyId)
    {
        var qualities = await  _qualityService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<QualityModelResponse>>(qualities);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var quality = await _qualityService.GetById(id);
        if (quality == null)
            return BadRequest("Знание с таким id не найдено");
        await _qualityService.Delete(quality);
        return NoContent();
    }

    [HttpPost("create")]
    public async Task<ActionResult> Post(QualityModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _qualityService.IsExists(model.Name))
            return BadRequest("Данное качество уже существует");
        
        var quality = _mapper.Map<Quality>(model);
        await _qualityService.Create(quality);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(QualityModelRequest model)
    {
       if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var quality = _mapper.Map<Quality>(model);
        bool check =  await _qualityService.Update(quality);
        if (!check)
            return BadRequest("нет такого качества");
        
        return NoContent();
    }
}