using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.EnumStates;
using HRCRM.Models.ViewModels.Application;
using HRCRM.Services.ApplicationServices;
using HRCRM.Services.BenefitServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class ApplicationsController : ControllerBase
{
    private readonly IApplicationService _applicationService;
    private readonly IBenefitService _benefitService;
    private readonly IMapper _mapper;

    public ApplicationsController(IApplicationService applicationService, IMapper mapper, IBenefitService benefitService)
    {
        _applicationService = applicationService;
        _mapper = mapper;
        _benefitService = benefitService;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ApplicationModelResponse>>> Get()
    {
        var applications = await _applicationService.GetAll();
        var result = _mapper.Map<IEnumerable<ApplicationModelResponse>>(applications);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ApplicationModelResponse>> Get(int id)
    {
        var application = await _applicationService.GetById(id);
        if (application == null)
            return BadRequest("Заявка с таким id не найдена");
        var result = _mapper.Map<ApplicationModelResponse>(application);
        return Ok(result);
    }

    [HttpDelete]
    public async Task<ActionResult> Delete(int id)
    {
        var application = await _applicationService.GetById(id);
        if (application == null)
            return BadRequest("Заявка с таким id не найдена");
        await _applicationService.Delete(application);
        return NoContent();
    }

    [HttpPut]
    public async Task<ActionResult> Put(ApplicationModelRequest model)
    {
        var application = await _applicationService.GetById(model.Id);
        if (application == null)
            return BadRequest("Заявка с таким id не найдена");
        
        // application = _mapper.Map<Application>(model);
        application.Position = model.Position;
        application.CityId = model.CityId;
        application.DivisionId = model.DivisionId;
        application.DepartmentId = model.DepartmentId;
        application.LineManagerId = model.LineManagerId;
        application.OpenReason = model.OpenReason;
        application.LaborContract = model.LaborContract;
        application.EnglishProficiency = model.EnglishProficiency;
        application.Education = model.Education;
        application.OtherKnowledge = model.OtherKnowledge;
        application.Salary = model.Salary;
        application.CompanyId = model.CompanyId;
        application.StatusApplication = model.StatusApplication;
        
        List<Benefit> benefits = new List<Benefit>();
        foreach (var benefitId in model.BenefitIds)
        {
            var benefit = await _benefitService.GetById(benefitId);
            if (benefit == null)
                return BadRequest("Льгота с таким id не найдена");
            benefits.Add(benefit);
        }

        application.Benefits = benefits;
        await _applicationService.Update(application);
        return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult> Post(ApplicationModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        var application = _mapper.Map<Application>(model);
        
        List<Benefit> benefits = new List<Benefit>();
        foreach (var benefitId in model.BenefitIds)
        {
            var benefit = await _benefitService.GetById(benefitId);
            if (benefit == null)
                return BadRequest("Льгота с таким id не найдена");
            benefits.Add(benefit);
        }

        application.Benefits = benefits;
        await _applicationService.Create(application);
        return NoContent();
    }
}