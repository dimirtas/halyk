﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.User;
using HRCRM.Services.UserServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public UsersController(IUserService userService, IMapper mapper)
    {
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<UserModelResponse>>> Get()
    {
        var users = await _userService.GetAll();
        var result = _mapper.Map<IEnumerable<UserModelResponse>>(users);
        return Ok(result);
    }
    
    [HttpGet("{id:int}")]
    public async Task<ActionResult<UserModelResponse>> Get(int id)
    {
        var user = await _userService.FindById(id);
        if (user is null)
            return BadRequest("Пользователь с таким id не найден");
        var result = _mapper.Map<UserModelResponse?>(user);
        return Ok(result);
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(UserModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool checkResponse = await _userService.Update(model);

        if (!checkResponse)
            return BadRequest("Пользователь не найден");

        return NoContent();
    }
    
    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var checkResponse = await _userService.Delete(id);

        if (!checkResponse)
            return BadRequest("Нет такого пользователя");
        
        return NoContent(); 
    }
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<User>>> GetByCompanyId(int companyId)
    {
        var users = await _userService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<UserModelResponse>>(users);
        return Ok(result);
    }

    [HttpPost("changePassword")]
    public async Task<ActionResult> ChangePassword(UserModelPassword model)
    {
        User? user = await _userService.FindByEmail(model.Email);

        if (user is null || user.IsDeleted)
            return BadRequest("Пользователя с таким email не существует");
        
        bool success = await _userService.ChangePassword(user, model.Email, model.checkingCurrentPassword, model.newPassword);

        if (!success)
            return BadRequest("Текущий пароль неверный");

        return Ok("Пароль успешно изменен");
    }

    [HttpGet("remindPassword")]
    public async Task<ActionResult> RemindPassword(string email)
    {
        User? user = await _userService.FindByEmail(email);
        
        if (user is null || user.IsDeleted)
            return BadRequest("Пользователя с таким email не существует");

        await _userService.RemindPassword(user, email);

        return Ok("Новый пароль был отправлен на Ваш адрес электронной почты.");
    }
}