using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels.Application;
using HRCRM.Models.ViewModels.Vacancy;
using HRCRM.Services.AbilityServices;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.KnowledgeServices;
using HRCRM.Services.QualityServices;
using HRCRM.Services.SkillServices;
using HRCRM.Services.VacancyServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class VacanciesController : ControllerBase
{
    private readonly IVacancyService _vacancyService;
    private readonly IBenefitService _benefitService;
    private readonly IKnowledgeService _knowledgeService;
    private readonly IAbilityService _abilityService;
    private readonly IQualityService _qualityService;
    private readonly ISkillService _skillService;
    private readonly IMapper _mapper;


    public VacanciesController(IVacancyService vacancyService, IBenefitService benefitService, IKnowledgeService knowledgeService, IAbilityService abilityService, IQualityService qualityService, ISkillService skillService, IMapper mapper)
    {
        _vacancyService = vacancyService;
        _benefitService = benefitService;
        _knowledgeService = knowledgeService;
        _abilityService = abilityService;
        _qualityService = qualityService;
        _skillService = skillService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<VacancyModelResponse>>> Get()
    {
        var vacancies = await _vacancyService.GetAll();
        var result = _mapper.Map<IEnumerable<VacancyModelResponse>>(vacancies);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<VacancyModelResponse>> Get(int id)
    {
        var vacancy = await _vacancyService.GetById(id);
        if (vacancy == null)
            return BadRequest("Заявка с таким id не найдена");
        var result = _mapper.Map<VacancyModelResponse>(vacancy);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var vacancy = await _vacancyService.GetById(id);
        if (vacancy == null)
            return BadRequest("Вакансия с таким id не найдена");
        await _vacancyService.Delete(vacancy);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Put(VacancyModelRequest model)
    {
        var vacancy = await _vacancyService.GetById(model.Id);
        if (vacancy == null)
            return BadRequest("Вакансия с таким id не найдена");

        vacancy.Position = model.Position;
        vacancy.CityId = model.CityId;
        vacancy.DivisionId = model.DivisionId;
        vacancy.DepartmentId = model.DepartmentId;
        vacancy.PositionDirectManagerId = model.PositionDirectManagerId;
        vacancy.ManagerOfTheManagerId = model.ManagerOfTheManagerId;
        vacancy.CountOfEmployees = model.CountOfEmployees;
        vacancy.Responsibilities = model.Responsibilities;
        vacancy.ExpectedResults= model.ExpectedResults;
        vacancy.Abilities = vacancy.Abilities;
        vacancy.Kpi = model.Kpi;
        vacancy.CareerOpportunities = model.CareerOpportunities;
        vacancy.CommentCareerOpportunities = model.CommentCareerOpportunities;
        vacancy.PositionMission = model.PositionMission;
        vacancy.Requirements = model.Requirements;
        vacancy.Salary = model.Salary;
        vacancy.OpenReason = model.OpenReason;
        vacancy.LaborContract = model.LaborContract;
        vacancy.EnglishProficiency = model.EnglishProficiency;
        vacancy.Education = model.Education;
        vacancy.WorkingСonditions = model.WorkingСonditions;
        vacancy.WorkExperience = model.WorkExperience;
        vacancy.CompanyId = model.CompanyId;
        
        List<Benefit> benefits = new List<Benefit>();
        foreach (var benefitId in model.BenefitIds)
        {
            var benefit = await _benefitService.GetById(benefitId);
            if (benefit == null)
                return BadRequest("Льгота с таким id не найдена");
            benefits.Add(benefit);
        }
        vacancy.Benefits = benefits;

        List<Knowledge> knowledges = new List<Knowledge>();
        foreach (var knowledgeId in model.KnowledgeIds)
        {
            var knowledge = await _knowledgeService.GetById(knowledgeId);
            if (knowledge == null)
                return BadRequest("Знание с таким id не найдено");
            knowledges.Add(knowledge);
        }
        vacancy.Knowledges = knowledges;
       
        List<Skill> skills = new List<Skill>();
        foreach (var skillIds in model.SkillIds)
        {
            var skill = await _skillService.GetById(skillIds);
            if (skill == null)
                return BadRequest("Навык с таким id не найден");
            skills.Add(skill);
        }
        vacancy.Skills = skills;
      
        List<Ability> abilities = new List<Ability>();
        foreach (var abilityIds in model.AbilityIds)
        {
            var ability = await _abilityService.GetById(abilityIds);
            if (ability == null)
                return BadRequest("Способность с таким id не найдена");
            abilities.Add(ability);
        }
        vacancy.Abilities = abilities;
    
        List<Quality> qualities = new List<Quality>();
        foreach (var qualityIds in model.QualityIds)
        {
            var quality = await _qualityService.GetById(qualityIds);
            if (quality == null)
                return BadRequest("Личностные качества с таким id не найдены");
            qualities.Add(quality);
        }
        vacancy.Qualities = qualities;

        await _vacancyService.Update(vacancy);
        return NoContent();
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Post(VacancyModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        var vacancy = _mapper.Map<Vacancy>(model);
        
        List<Benefit> benefits = new List<Benefit>();
        foreach (var benefitId in model.BenefitIds)
        {
            var benefit = await _benefitService.GetById(benefitId);
            if (benefit == null)
                return BadRequest("Льгота с таким id не найдена");
            benefits.Add(benefit);
        }
        vacancy.Benefits = benefits;

        List<Knowledge> knowledges = new List<Knowledge>();
        foreach (var knowledgeId in model.KnowledgeIds)
        {
            var knowledge = await _knowledgeService.GetById(knowledgeId);
            if (knowledge == null)
                return BadRequest("Знание с таким id не найдено");
            knowledges.Add(knowledge);
        }
        vacancy.Knowledges = knowledges;
       
        List<Skill> skills = new List<Skill>();
        foreach (var skillIds in model.SkillIds)
        {
            var skill = await _skillService.GetById(skillIds);
            if (skill == null)
                return BadRequest("Навык с таким id не найден");
            skills.Add(skill);
        }
        vacancy.Skills = skills;
      
        List<Ability> abilities = new List<Ability>();
        foreach (var abilityIds in model.AbilityIds)
        {
            var ability = await _abilityService.GetById(abilityIds);
            if (ability == null)
                return BadRequest("Способность с таким id не найдена");
            abilities.Add(ability);
        }
        vacancy.Abilities = abilities;
    
        List<Quality> qualities = new List<Quality>();
        foreach (var qualityIds in model.QualityIds)
        {
            var quality = await _qualityService.GetById(qualityIds);
            if (quality == null)
                return BadRequest("Личностные качества с таким id не найдены");
            qualities.Add(quality);
        }
        vacancy.Qualities = qualities;

        await _vacancyService.Create(vacancy);
        return NoContent();
    }
    
    [HttpPost("createFromApp")]
    public async Task<ActionResult> Post(ApplicationModelRequest model)
    {
        var vacancy = _mapper.Map<Vacancy>(model);
        
        List<Benefit> benefits = new List<Benefit>();
        foreach (var benefitId in model.BenefitIds)
        {
            var benefit = await _benefitService.GetById(benefitId);
            if (benefit == null)
                return BadRequest("Льгота с таким id не найдена");
            benefits.Add(benefit);
        }
        vacancy.Benefits = benefits;
        
        await _vacancyService.Create(vacancy);
        
        var vacancyResponce = await _vacancyService.GetById(vacancy.Id);
        if (vacancyResponce == null)
                return BadRequest("Вакансия с таким id не найдена");
        var result = _mapper.Map<VacancyModelResponse>(vacancyResponce);
        return Ok(result);
    }
}

