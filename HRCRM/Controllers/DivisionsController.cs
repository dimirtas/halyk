using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.DivisionServices;
using Microsoft.AspNetCore.Mvc;


namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class DivisionsController : ControllerBase
{
    private readonly IDivisionService _divisionService;
    private readonly IMapper _mapper;


    public DivisionsController(IDivisionService divisionService, IMapper mapper)
    {
        _divisionService = divisionService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Division>>> Get()
    {
        var divisions = await  _divisionService.GetAll();
        var result =  _mapper.Map<IEnumerable<DivisionViewModel>>(divisions);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<DivisionViewModel>> Get(int id)
    {
        var division = await _divisionService.GetById(id);
        if (division is null)
            return BadRequest("Дивизиона с таким id не найден");
        var result = _mapper.Map<DivisionViewModel>(division);
        return Ok(result);
    }
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Division>>> GetByCompanyId(int companyId)
    {
        var divisions = await  _divisionService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<DivisionViewModel>>(divisions);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(DivisionCreateViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _divisionService.IsExists(model.Name))
            return BadRequest("Данный дивизион уже существует");

        await _divisionService.Create(model);
        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _divisionService.Delete(id);
        if (!check)
            return BadRequest("Нет такого дивизиона");
        
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(DivisionViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool check =  await _divisionService.Update(model);
        if (!check)
            return BadRequest("Нет такого дивизиона");
        
        return NoContent();
    }
}