﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels.Resume;
using HRCRM.Services.EmailServices;
using HRCRM.Services.ResumeServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ResumesController : ControllerBase
{
    private readonly IResumeService _resumeService;
    private readonly IEmailService _emailService;
    private readonly IMapper _mapper;

    public ResumesController(IResumeService resumeService, IMapper mapper, IEmailService emailService)
    {
        _resumeService = resumeService;
        _mapper = mapper;
        _emailService = emailService;
    }
    
    [HttpPost("GetMessages")]
    public async Task<ActionResult> GetMessages(int id)
    {
        await _emailService.GetNewMessages(id);
        return NoContent();
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ResumeModelResponse>>> Get()
    {
        var resumes = await _resumeService.GetAll();
        var result = _mapper.Map<IEnumerable<ResumeModelResponse>>(resumes);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ResumeModelResponse>> Get(int id)
    {
        var resume = await _resumeService.GetById(id);
        
        if (resume == null)
            return BadRequest("Резюме с таким id не существует");
        
        var result = _mapper.Map<ResumeModelResponse>(resume);
        return Ok(result);
    }

    [HttpPost("create")]
    public async Task<ActionResult> Post(ResumeModelRequest resumeModel)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        await _resumeService.Create(resumeModel);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Put(ResumeModelRequest resumeModel)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool checkResponse = await _resumeService.Update(resumeModel);

        if (!checkResponse)
            return BadRequest("Резюме с таким id не существует");

        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var checkResponse = await _resumeService.Delete(id);

        if (!checkResponse)
            return BadRequest("Резюме с таким id не существует");
        
        return NoContent(); 
    }
}