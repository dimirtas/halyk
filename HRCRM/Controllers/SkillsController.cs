using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.SkillServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class SkillsController : ControllerBase
{
    private readonly ISkillService _skillService;
    private readonly IMapper _mapper;

    public SkillsController(ISkillService skillService, IMapper mapper)
    {
        _skillService = skillService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<SkillModelResponse>>> Get()
    {
        var skills = await _skillService.GetAll();
        var result = _mapper.Map<IEnumerable<SkillModelResponse>>(skills);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<SkillModelResponse>> Get(int id)
    {
        var skill = await _skillService.GetById(id);
        if (skill == null)
            return BadRequest("Навыка с таким id не найдено");
        var result = _mapper.Map<SkillModelResponse>(skill);
        return Ok(result);
    }
    
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Skill>>> GetByCompanyId(int companyId)
    {
        var skills = await  _skillService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<SkillModelResponse>>(skills);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var skill = await _skillService.GetById(id);
        if (skill == null)
            return BadRequest("Навыка с таким id не найдено");
        await _skillService.Delete(skill);
        return NoContent();
    }

    [HttpPost("create")]
    public async Task<ActionResult> Post(SkillModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _skillService.IsExists(model.Name))
            return BadRequest("Данный навык уже существует");
        
        var skill = _mapper.Map<Skill>(model);
        await _skillService.Create(skill);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(SkillModelRequest model)
    {
       if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var skill = _mapper.Map<Skill>(model);
        bool check =  await _skillService.Update(skill);
        if (!check)
            return BadRequest("нет такого навыка");
        
        return NoContent();
    }
}