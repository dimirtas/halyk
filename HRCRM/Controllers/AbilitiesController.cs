using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.AbilityServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class AbilitiesController : ControllerBase
{
    private readonly IAbilityService _abilityService;
    private readonly IMapper _mapper;


    public AbilitiesController(IAbilityService abilityService, IMapper mapper)
    {
        _abilityService = abilityService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<AbilityModelResponse>>> Get()
    {
        var abilities = await _abilityService.GetAll();
        var result = _mapper.Map<IEnumerable<AbilityModelResponse>>(abilities);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<AbilityModelResponse>> Get(int id)
    {
        var ability = await _abilityService.GetById(id);
        if (ability == null)
            return BadRequest("Способности с таким id не найдено");
        var result = _mapper.Map<AbilityModelResponse>(ability);
        return Ok(result);
    }
    
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Ability>>> GetByCompanyId(int companyId)
    {
        var abilities = await  _abilityService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<AbilityModelResponse>>(abilities);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var ability = await _abilityService.GetById(id);
        if (ability == null)
            return BadRequest("Способности с таким id не найдено");
        await _abilityService.Delete(ability);
        return NoContent();
    }

    [HttpPost("create")]
    public async Task<ActionResult> Post(AbilityModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _abilityService.IsExists(model.Name))
            return BadRequest("Данная способность уже существует");
        
        var ability = _mapper.Map<Ability>(model);
        await _abilityService.Create(ability);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(AbilityModelRequest model)
    {
       if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var ability = _mapper.Map<Ability>(model);
        bool check =  await _abilityService.Update(ability);
        if (!check)
            return BadRequest("нет такой способности");
        
        return NoContent();
    }
}