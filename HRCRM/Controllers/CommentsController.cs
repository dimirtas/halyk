﻿using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels.Application;
using HRCRM.Models.ViewModels.Commit;
using HRCRM.Models.ViewModels.Vacancy;
using HRCRM.Services.CommentServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class CommentsController : ControllerBase
{
    private readonly ICommentService _commentService;
    private readonly IMapper _mapper;

    public CommentsController(ICommentService commentService, IMapper mapper)
    {
        _commentService = commentService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<CommentModelResponse>>> Get()
    {
        var comments = await _commentService.GetAll();
        var result = _mapper.Map<IEnumerable<CommentModelResponse>>(comments);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<CommentModelResponse>> Get(int id)
    {
        var comment = await _commentService.GetById(id);
        if (comment == null)
            return BadRequest("Комментарий  с таким id не найден");
        var result = _mapper.Map<CommentModelResponse>(comment);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var comment = await _commentService.GetById(id);
        if (comment == null)
            return BadRequest("Комментарий  с таким id не найден");
        await _commentService.Delete(comment);
        return NoContent();
    }

    [HttpPut("update")]
    public async Task<ActionResult> Update(CommentModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var comment = _mapper.Map<Comment>(model);
        bool check =  await _commentService.Update(comment);
        if (!check)
            return BadRequest("Комментарий  с таким id не найден");
        
        return NoContent();
    }


    [HttpPost("create")]
    public async Task<ActionResult> Post(CommentModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var comment = _mapper.Map<Comment>(model);
        await _commentService.Create(comment);
        return NoContent();
    }

}