using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.BenefitServices;
using Microsoft.AspNetCore.Mvc;


namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class BenefitsController : ControllerBase
{
    private readonly IBenefitService _benefitService;
    private readonly IMapper _mapper;


    public BenefitsController(IBenefitService benefitService, IMapper mapper)
    {
        _benefitService = benefitService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Benefit>>> Get()
    {
        var benefits = await  _benefitService.GetAll();
        var result =  _mapper.Map<IEnumerable<BenefitViewModel>>(benefits);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<BenefitViewModel>> Get(int id)
    {
        var benefit = await _benefitService.GetById(id);
        if (benefit is null)
            return BadRequest("Льгота с таким id не найдена");
        var result = _mapper.Map<BenefitViewModel>(benefit);
        return Ok(result);
    }
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Benefit>>> GetByCompanyId(int companyId)
    {
        var benefits = await  _benefitService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<BenefitViewModel>>(benefits);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(BenefitCreateViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _benefitService.IsExists(model.Name))
            return BadRequest("Данная льгота уже существует");

        await _benefitService.Create(model);
        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _benefitService.Delete(id);
        if (!check)
            return BadRequest("Нет такой льготы");
        
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(BenefitViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool check =  await _benefitService.Update(model);
        if (!check)
            return BadRequest("Нет такой льготы");
        
        return NoContent();
    }
}