using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.KnowledgeServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class KnowledgesController : ControllerBase
{
    private readonly IKnowledgeService _knowledgeService;
    private readonly IMapper _mapper;


    public KnowledgesController(IKnowledgeService knowledgeService, IMapper mapper)
    {
        _knowledgeService = knowledgeService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<KnowledgeModelResponse>>> Get()
    {
        var knowledges = await _knowledgeService.GetAll();
        var result = _mapper.Map<IEnumerable<KnowledgeModelResponse>>(knowledges);
        return Ok(result);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<KnowledgeModelResponse>> Get(int id)
    {
        var knowledge = await _knowledgeService.GetById(id);
        if (knowledge == null)
            return BadRequest("Знание с таким id не найдено");
        var result = _mapper.Map<KnowledgeModelResponse>(knowledge);
        return Ok(result);
    }
    
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Knowledge>>> GetByCompanyId(int companyId)
    {
        var knowledges = await  _knowledgeService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<KnowledgeModelResponse>>(knowledges);
        return Ok(result);
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        var knowledge = await _knowledgeService.GetById(id);
        if (knowledge == null)
            return BadRequest("Знание с таким id не найдено");
        await _knowledgeService.Delete(knowledge);
        return NoContent();
    }

    [HttpPost("create")]
    public async Task<ActionResult> Post(KnowledgeModelRequest model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _knowledgeService.IsExists(model.Name))
            return BadRequest("Данное знание уже существует");
        
        var knowledge = _mapper.Map<Knowledge>(model);
        await _knowledgeService.Create(knowledge);
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(KnowledgeModelRequest model)
    {
       if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var knowledge = _mapper.Map<Knowledge>(model);
        bool check =  await _knowledgeService.Update(knowledge);
        if (!check)
            return BadRequest("нет таких знаний");
        
        return NoContent();
    }
}