using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CityServices;
using Microsoft.AspNetCore.Mvc;


namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class CitiesController : ControllerBase
{
    private readonly ICityService _cityService;
    private readonly IMapper _mapper;


    public CitiesController(ICityService cityService, IMapper mapper)
    {
        _cityService = cityService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<City>>> Get()
    {
        var cities = await _cityService.GetAll();
        var result = _mapper.Map<IEnumerable<CityViewModel>>(cities);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<CityViewModel>> Get(int id)
    {
        var city = await _cityService.GetById(id);
        if (city is null)
            return BadRequest("Город с таким id не найден");
        var result = _mapper.Map<CityViewModel>(city);
        return Ok(result);
    }
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<City>>> GetByCompanyId(int companyId)
    {
        var cities = await  _cityService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<CityViewModel>>(cities);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(CityCreateViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _cityService.IsExists(model.Name))
            return BadRequest("Данный город уже существует");

        await _cityService.Create(model);
        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _cityService.Delete(id);
        if (!check)
            return BadRequest("Нет такого города");
        
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(CityViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool check =  await _cityService.Update(model);
        if (!check)
            return BadRequest("Нет такого города");
        
        return NoContent();
    }
}