using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.CompanyServices;
using Microsoft.AspNetCore.Mvc;

namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class CompaniesController : ControllerBase
{
    private readonly ICompanyService _companyService;
    private readonly IMapper _mapper;

    public CompaniesController(ICompanyService companyService, IMapper mapper)
    {
        _companyService = companyService;
        _mapper = mapper;
    }
    
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Company>>> Get()
    {
        var companies = await _companyService.GetAllCompanies();
        var result =  _mapper.Map<IEnumerable<CompanyViewModel>>(companies);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<CompanyViewModel>> Get(int id)
    {
        var company = await _companyService.GetById(id);
        if (company is null)
            return BadRequest("Организация с таким id не найдена");
        var result = _mapper.Map<CompanyViewModel>(company);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(CompanyCreateViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _companyService.IsExists(model.Bin))
            return BadRequest("Данная организация уже существует");

        await _companyService.Create(model);
        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _companyService.Delete(id);
        if (!check)
            return BadRequest("Нет такой организации");
        
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(CompanyViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool check =  await _companyService.Update(model);
        if (!check)
            return BadRequest("Нет такой организации");
        
        return NoContent();
    }
}