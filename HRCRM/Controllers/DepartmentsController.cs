using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.DepartmentServices;
using Microsoft.AspNetCore.Mvc;


namespace HRCRM.Controllers;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class DepartmentsController : ControllerBase
{
    private readonly IDepartmentService _departmentService;
    private readonly IMapper _mapper;


    public DepartmentsController(IDepartmentService departmentService, IMapper mapper)
    {
        _departmentService = departmentService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Department>>> Get()
    {
        var departments = await  _departmentService.GetAll();
        var result =  _mapper.Map<IEnumerable<DepartmentViewModel>>(departments);
        return Ok(result);
    }

    [HttpGet("{id:int}")] 
    public async Task<ActionResult<DepartmentViewModel>> Get(int id)
    {
        var department = await _departmentService.GetById(id);
        if (department is null)
            return BadRequest("Отдел с таким id не найден");
        var result = _mapper.Map<DepartmentViewModel>(department);
        return Ok(result);
    }
    
    [HttpGet("GetByCompanyId/{companyId}")]
    public async Task<ActionResult<IEnumerable<Department>>> GetByCompanyId(int companyId)
    {
        var departments = await  _departmentService.GetByCompanyId(companyId);
        var result =  _mapper.Map<IEnumerable<DepartmentViewModel>>(departments);
        return Ok(result);
    }
    
    [HttpPost("create")]
    public async Task<ActionResult> Create(DepartmentCreateViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        
        if (await _departmentService.IsExists(model.Name))
            return BadRequest("Данный отдел уже существует");

        await _departmentService.Create(model);
        return NoContent();
    }

    [HttpDelete("delete")]
    public async Task<ActionResult> Delete(int id)
    {
        bool check =  await _departmentService.Delete(id);
        if (!check)
            return BadRequest("Нет такого отдела");
        
        return NoContent();
    }
    
    [HttpPut("update")]
    public async Task<ActionResult> Update(DepartmentViewModel model)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        bool check =  await _departmentService.Update(model);
        if (!check)
            return BadRequest("Нет такого отдела");
        
        return NoContent();
    }
}