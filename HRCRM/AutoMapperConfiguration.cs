using AutoMapper;
using HRCRM.Models;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.Application;
using HRCRM.Models.ViewModels.Commit;
using HRCRM.Models.ViewModels.Resume;
using HRCRM.Models.ViewModels.User;
using HRCRM.Models.ViewModels.Vacancy;

namespace HRCRM;

public class AutoMapperConfiguration : Profile
{
    public AutoMapperConfiguration()
    {
        CreateMap<Company, CompanyViewModel>().ReverseMap();
        CreateMap<CompanyCreateViewModel, Company>().ReverseMap();
        CreateMap<Role, RoleViewModel>().ReverseMap();
        CreateMap<RoleNewViewModel, Role>().ReverseMap();
        CreateMap<User, UserModelResponse>()
            .ForMember("RoleIds", opt => opt.MapFrom(src => src.Roles));
        CreateMap<UserModelResponse, User>();
        CreateMap<RegisterViewModel, User>()
            .ForMember("Username", opt => opt.MapFrom(src => src.Email));
        CreateMap<UserModelRequest, User>().ReverseMap();
        CreateMap<City, CityViewModel>().ReverseMap();
        CreateMap<CityCreateViewModel, City>().ReverseMap();
        CreateMap<Benefit, BenefitViewModel>().ReverseMap();
        CreateMap<BenefitCreateViewModel, Benefit>().ReverseMap();
        CreateMap<Department, DepartmentViewModel>().ReverseMap();
        CreateMap<DepartmentCreateViewModel, Department>().ReverseMap();
        CreateMap<Division, DivisionViewModel>().ReverseMap();
        CreateMap<DivisionCreateViewModel, Division>().ReverseMap();
        CreateMap<Application, ApplicationModelRequest>().ReverseMap();
        CreateMap<Application, ApplicationModelResponse>()
            .ForMember("BenefitIds", opt => opt.MapFrom(src => src.Benefits)).ReverseMap();
        CreateMap<Knowledge, KnowledgeModelRequest>().ReverseMap();
        CreateMap<Knowledge, KnowledgeModelResponse>().ReverseMap();
        CreateMap<Quality, QualityModelResponse>().ReverseMap();
        CreateMap<Quality, QualityModelRequest>().ReverseMap();
        CreateMap<Skill, SkillModelResponse>().ReverseMap();
        CreateMap<Skill, SkillModelRequest>().ReverseMap();
        CreateMap<Ability, AbilityModelResponse>().ReverseMap();
        CreateMap<Ability, AbilityModelRequest>().ReverseMap();
        CreateMap<Vacancy, VacancyModelRequest>().ReverseMap();
        CreateMap<Vacancy, VacancyModelResponse>()
            .ForMember("KnowledgeIds", opt => opt.MapFrom(src => src.Knowledges))
            .ForMember("SkillIds", opt => opt.MapFrom(src => src.Skills))
            .ForMember("AbilityIds", opt => opt.MapFrom(src => src.Abilities))
            .ForMember("QualityIds", opt => opt.MapFrom(src => src.Qualities))
            .ForMember("BenefitIds", opt => opt.MapFrom(src => src.Benefits))
            .ReverseMap();
        CreateMap<Comment, CommentModelResponse>().ReverseMap();
        CreateMap<Comment, CommentModelRequest>().ReverseMap();
        CreateMap<Resume, ResumeModelRequest>().ReverseMap();
        CreateMap<Resume, ResumeModelResponse>().ReverseMap();
        CreateMap<ApplicationModelRequest, Vacancy>()
            .ForMember("PositionDirectManagerId", opt => opt.MapFrom(src => src.LineManagerId))
            .ForMember(dest => dest.Id, act => act.Ignore())
            .ReverseMap();
    }
}