﻿using HRCRM.Services.AbilityServices;
using HRCRM.Services.ApplicationServices;
using HRCRM.Services.AuthServices;
using HRCRM.Services.BenefitServices;
using HRCRM.Services.CityServices;
using HRCRM.Services.CommentServices;
using HRCRM.Services.CompanyServices;
using HRCRM.Services.DepartmentServices;
using HRCRM.Services.DivisionServices;
using HRCRM.Services.EmailServices;
using HRCRM.Services.KnowledgeServices;
using HRCRM.Services.LoggerService;
using HRCRM.Services.PasswordServices;
using HRCRM.Services.QualityServices;
using HRCRM.Services.ResumeServices;
using HRCRM.Services.RoleServices;
using HRCRM.Services.SkillServices;
using HRCRM.Services.TokenServices;
using HRCRM.Services.UserServices;
using HRCRM.Services.VacancyServices;
using LoggerService;

namespace HRCRM.Extensions;

public static class AppServices
{
    public static void AddServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IRoleService, RoleService>();
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<ITokenService, TokenService>();
        services.AddScoped<IPasswordService, PasswordService>();
        services.AddScoped<IEmailService, EmailService>();
        services.AddScoped<ICompanyService, CompanyService>();
        services.AddScoped<ICityService, CityService>();
        services.AddScoped<IBenefitService, BenefitService>();
        services.AddScoped<IDepartmentService, DepartmentService>();
        services.AddScoped<IDivisionService, DivisionService>();
        services.AddScoped<IApplicationService, ApplicationService>();
        services.AddScoped<IKnowledgeService, KnowledgeService>();
        services.AddScoped<IQualityService, QualityService>();
        services.AddScoped<ISkillService, SkillService>();
        services.AddScoped<IAbilityService, AbilityService>();
        services.AddScoped<IVacancyService, VacancyService>();
        services.AddScoped<ICommentService, CommentService>();
        services.AddSingleton<ILoggerManager, LoggerManager>();
        services.AddScoped<IResumeService, ResumeService>();
    }
}