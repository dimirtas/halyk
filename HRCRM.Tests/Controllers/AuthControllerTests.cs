using System.Threading.Tasks;
using HRCRM.Controllers;
using HRCRM.Models.ViewModels;
using HRCRM.Models.ViewModels.Auth;
using HRCRM.Services.AuthServices;
using HRCRM.Services.TokenServices;
using HRCRM.Services.UserServices;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;

namespace HRCRM.Tests.Controllers;

public class AuthControllerTests
{
    [Fact]
    public async Task LoginSuccessful()
    {
        var authService = new Mock<IAuthService>();
        var tokenService = new Mock<ITokenService>();
        var userService = new Mock<IUserService>();
        var authController = new AuthController(authService.Object, tokenService.Object, userService.Object);
    
        var model = new LoginViewModel();
        
        authService.Setup(s => s.Login(model)).ReturnsAsync(new LoginModelResponse());
    
        var result = await authController.Login(model) as ObjectResult;
        Assert.IsType<OkObjectResult>(result);
        Assert.IsType<LoginModelResponse>(result.Value);
        Assert.NotNull(result.Value);
    }

    [Fact]
    public async Task Reqister()
    {
        var authService = new Mock<IAuthService>();
        var tokenService = new Mock<ITokenService>();
        var userService = new Mock<IUserService>();
        var authController = new AuthController(authService.Object, tokenService.Object, userService.Object);

        var model = new RegisterViewModel(){Email = "some email", Phone = "8777"};
        
        userService.Setup(s => s.IsEmailExists(model.Email)).ReturnsAsync(false);
        userService.Setup(s => s.IsPhoneNumberExists(model.Phone)).ReturnsAsync(false);
        authService.Setup(s => s.Register(model));

        var result = await authController.Register(model) as ObjectResult;

        Assert.Null(result);
    }

    [Fact]
    public async Task Logout()
    {
        var authService = new Mock<IAuthService>();
        var tokenService = new Mock<ITokenService>();
        var userService = new Mock<IUserService>();
        var authController = new AuthController(authService.Object, tokenService.Object, userService.Object);

        authService.Setup(s => s.Logout(1)).ReturnsAsync(true);

        var result = await authController.Logout(1) as ObjectResult;
        
        Assert.Null(result);
    }
    
    [Fact]
    public async Task LogoutFailed()
    {
        var authService = new Mock<IAuthService>();
        var tokenService = new Mock<ITokenService>();
        var userService = new Mock<IUserService>();
        var authController = new AuthController(authService.Object, tokenService.Object, userService.Object);

        authService.Setup(s => s.Logout(1)).ReturnsAsync(false);

        var result = await authController.Logout(1) as ObjectResult;
        
        Assert.NotNull(result);
        Assert.IsType<BadRequestObjectResult>(result);
    }
}